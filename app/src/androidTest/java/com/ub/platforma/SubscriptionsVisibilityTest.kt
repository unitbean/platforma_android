package com.ub.platforma

import android.view.View
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import com.agoda.kakao.bottomnav.KBottomNavigationView
import com.agoda.kakao.image.KImageView
import com.agoda.kakao.recycler.KRecyclerItem
import com.agoda.kakao.recycler.KRecyclerView
import com.agoda.kakao.screen.Screen
import com.agoda.kakao.text.KTextView
import com.ub.platforma.ui.main.activities.MainActivity
import org.hamcrest.Matcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class SubscriptionsVisibilityTest : Screen<SubscriptionsVisibilityTest>() {

    @Rule
    @JvmField
    val rule = ActivityTestRule(MainActivity::class.java)

    val partnersRecycler: KRecyclerView by lazy {
        KRecyclerView(
            builder = {
                withId(R.id.rv_partners_data)
                isDisplayed()
            },
            itemTypeBuilder = { itemType(::PartnerHolder) }
        )
    }

    val subscriptionsRecycler: KRecyclerView by lazy {
        KRecyclerView(
            builder = {
                withId(R.id.rv_subscription)
                isDisplayed()
            },
            itemTypeBuilder = { itemType(::SubscriptionHeaderHolder) }
        )
    }

    val navigation = KBottomNavigationView {
        withId(R.id.bnv_main_navigation)
    }

    @Test
    fun testSubscriptionsIsShow() {
        onScreen<SubscriptionsVisibilityTest> {

            idle(duration = 2500)

            navigation.setSelectedItem(R.id.menu_partner)

            partnersRecycler {
                isVisible()
                scrollTo(4)
                childAt<PartnerHolder>(4) {
                    title.isVisible()
                    click()
                }
            }

            idle(duration = 1500)

            subscriptionsRecycler {
                isVisible()
                childAt<SubscriptionHeaderHolder>(0) {
                    closeButton.isVisible()
                    closeButton.click()
                }
            }
        }
    }

    class PartnerHolder(parent: Matcher<View>) : KRecyclerItem<PartnerHolder>(parent) {
        val title: KTextView = KTextView(parent) { withId(R.id.tv_partners_sponsor_title) }
    }

    class SubscriptionHeaderHolder(parent: Matcher<View>) : KRecyclerItem<SubscriptionHeaderHolder>(parent) {
        val closeButton: KImageView = KImageView(parent) { withId(R.id.iv_subscription_close) }
    }
}