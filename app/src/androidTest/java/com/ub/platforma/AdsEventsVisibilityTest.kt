package com.ub.platforma

import android.view.View
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import com.agoda.kakao.common.views.KView
import com.agoda.kakao.image.KImageView
import com.agoda.kakao.recycler.KRecyclerItem
import com.agoda.kakao.recycler.KRecyclerView
import com.agoda.kakao.screen.Screen
import com.agoda.kakao.text.KTextView
import com.ub.platforma.ui.main.activities.MainActivity
import org.hamcrest.Matcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class AdsEventsVisibilityTest : Screen<AdsEventsVisibilityTest>() {

    @Rule
    @JvmField
    val rule = ActivityTestRule(MainActivity::class.java)

    val eventsRecycler = KRecyclerView(
        builder = { withId(R.id.rv_events_list) },
        itemTypeBuilder = { itemType(::AdsHolder) }
    )

    @Test
    fun testAdvertisementShowInEvents() {
        onScreen<AdsEventsVisibilityTest> {

            idle(duration = 2500)

            eventsRecycler {
                isVisible()
                scrollTo(4)
                childAt<AdsHolder>(4) {
                    title.isVisible()
                }
            }
        }
    }

    class AdsHolder(parent: Matcher<View>) : KRecyclerItem<AdsHolder>(parent) {
        val title: KTextView = KTextView(parent) { withId(R.id.tv_events_ads_title) }
    }
}