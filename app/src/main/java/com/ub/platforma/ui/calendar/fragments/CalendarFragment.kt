package com.ub.platforma.ui.calendar.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.doOnPreDraw
import com.ub.platforma.R
import com.ub.platforma.ui.calendar.presenters.CalendarPresenter
import com.ub.platforma.ui.calendar.views.CalendarView
import com.ub.platforma.ui.event.fragments.EventFragment
import com.ub.platforma.ui.events.adapters.EventsRVAdapter
import com.ub.platforma.ui.events.fragments.EventsFragment.Companion.EVENT_MODEL
import com.ub.platforma.ui.events.interfaces.TransitionClickListener
import com.ub.platforma.ui.events.models.EventViewModel
import com.ub.platforma.ui.main.activities.MainActivity
import com.ub.platforma.ui.main.listeners.BackStackChangeListener
import com.ub.platforma.ui.subscription.fragments.SubscriptionFragment
import com.ub.platforma.utils.ScrollAware
import com.ub.utils.gone
import com.ub.utils.visible
import kotlinx.android.synthetic.main.component_toolbar.*
import kotlinx.android.synthetic.main.fragment_calendar.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter

class CalendarFragment : MvpAppCompatFragment(), CalendarView, TransitionClickListener, ScrollAware {

    @InjectPresenter lateinit var presenter: CalendarPresenter

    private val adapter: EventsRVAdapter by lazy {
        EventsRVAdapter(true, presenter.isPremiumUser).apply {
            this.transitionClickListener = this@CalendarFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_calendar, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as? MainActivity)?.setSupportActionBar(toolbar)
        (activity as? MainActivity)?.supportActionBar?.setTitle(R.string.calendar_title)

        postponeEnterTransition()

        (view.parent as? ViewGroup)?.doOnPreDraw {
            startPostponedEnterTransition()
            (activity as? BackStackChangeListener)?.onBackStackChange(this)
        }

        srl_calendar_events.setOnRefreshListener {
            presenter.updateUserEvents()
        }

        rv_calendar_events.setHasFixedSize(true)
        rv_calendar_events.adapter = adapter

        presenter.updateUserEvents()
    }

    override fun eventsUpdated(events: List<EventViewModel>) {
        srl_calendar_events.isRefreshing = false
        if (presenter.events.isNotEmpty()) {
            cl_calendar_empty.gone
            rv_calendar_events.visible
            adapter.update(events)
        } else {
            cl_calendar_empty.visible
            rv_calendar_events.gone
        }
    }

    override fun onShowError(message: String?) {
        srl_calendar_events.isRefreshing = false
        if (!message.isNullOrEmpty()) {
            tv_empty_text.text = message
        } else {
            tv_empty_text.text = getString(R.string.error_common)
        }
    }

    override fun onItemClick(view: View, position: Int, transitions: Map<String, View>?) {
        when (view.id) {
            R.id.cl_events_container -> (requireActivity() as MainActivity).openScreen(EventFragment.TAG, Bundle().apply {
                putParcelable(EVENT_MODEL, presenter.events[position])
            }, sharedElements = transitions)
            R.id.b_events_ads_button -> (requireActivity() as MainActivity).openScreen(SubscriptionFragment.TAG)
        }
    }

    override fun onScrollOnTop() {
        rv_calendar_events.smoothScrollToPosition(0)
    }

    companion object {
        const val TAG = "CalendarFragment"

        fun newInstance(): CalendarFragment {
            return CalendarFragment()
        }
    }
}