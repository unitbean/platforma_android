package com.ub.platforma.ui.base.presenters

import com.ub.platforma.ui.base.views.BaseView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import moxy.MvpPresenter
import kotlin.coroutines.CoroutineContext

open class BasePresenter<V : BaseView>: MvpPresenter<V>(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main
}