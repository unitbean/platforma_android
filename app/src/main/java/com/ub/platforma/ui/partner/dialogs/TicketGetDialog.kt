package com.ub.platforma.ui.partner.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.ub.platforma.R
import com.ub.platforma.ui.partner.presenters.TicketGetPresenter
import com.ub.platforma.ui.partner.views.TicketGetView
import com.ub.platforma.ui.partners.models.TicketOfferViewModel
import com.ub.utils.argument
import kotlinx.android.synthetic.main.dialog_ticket_get.*
import moxy.MvpBottomSheetDialogFragment
import moxy.presenter.InjectPresenter

class TicketGetDialog : MvpBottomSheetDialogFragment(), TicketGetView, View.OnClickListener {

    @InjectPresenter lateinit var presenter: TicketGetPresenter

    private val ticket: TicketOfferViewModel by argument(TICKET_EXTRA)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_ticket_get, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tv_ticket_get_title.text = String.format(getString(R.string.ticket_get_title_mask_first, view.context.resources.getQuantityString(R.plurals.tickets, ticket.countOfFreeTickets, ticket.countOfFreeTickets)))

        b_ticket_get_book.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.b_ticket_get_book -> presenter.validateAndBookTicket(et_ticket_get_name.text.toString(), et_ticket_get_phone.text.toString(), ticket)
        }
    }

    override fun onTicketBooked() {
        Toast.makeText(requireContext(), R.string.ticket_get_booking_success, Toast.LENGTH_LONG).show()
        dismissAllowingStateLoss()
    }

    override fun onShowError(resId: Int, details: String?) {
        val text = if (details.isNullOrEmpty()) {
            getString(resId)
        } else {
            String.format(getString(resId), details)
        }
        Toast.makeText(requireContext(), text, Toast.LENGTH_LONG).show()
    }

    companion object {
        const val TAG = "TicketGetDialog"
        const val TICKET_EXTRA = "ticket_extra"

        fun newInstance(): TicketGetDialog {
            return TicketGetDialog()
        }
    }
}