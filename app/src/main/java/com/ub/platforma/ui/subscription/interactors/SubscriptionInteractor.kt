package com.ub.platforma.ui.subscription.interactors

import com.platforma.repository.models.PremiumStatus
import com.platforma.repository.models.response.BuyLinkResponse
import com.ub.platforma.ui.subscription.models.*
import com.platforma.repository.subscription.ISubscriptionRepository

class SubscriptionInteractor(private val repository: ISubscriptionRepository) {

    suspend fun initialize(): List<ISubscription> {
        val models = ArrayList<ISubscription>()
        val tariffs = repository.receiveSubscriptions() ?: emptyList()

        models.add(HeaderViewModel())

        models.add(PromocodeViewModel())

        for (tariff in tariffs) {
            models.add(SubscriptionViewModel(tariff))
        }

        return models
    }

    suspend fun updateForStatus(@PremiumStatus status: Int): List<ISubscription> {
        val models = ArrayList<ISubscription>()
        val tariffs = repository.receiveSubscriptions() ?: emptyList()

        models.add(HeaderViewModel())

        for (tariff in tariffs) {
            models.add(SubscriptionViewModel(tariff))
        }

        return models
    }

    fun checkPromocodeAvailability(): Boolean {
        val isSign = repository.isUserSignIn()
        if (isSign) {
            repository.removeCachedProfile()
        }
        return isSign
    }

    fun checkSignIn(): Boolean {
        return repository.isUserSignIn()
    }

    suspend fun checkIsSignInAndGetBuyLink(subscription: SubscriptionViewModel): BuyLinkResponse? {
        return if (repository.isUserSignIn()) {
            repository.removeCachedProfile()
            repository.receiveBuyLink(subscription.id)
        } else null
    }
}