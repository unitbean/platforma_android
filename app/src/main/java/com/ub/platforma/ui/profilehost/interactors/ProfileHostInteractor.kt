package com.ub.platforma.ui.profilehost.interactors

import com.platforma.repository.profilehost.IProfileHostRepository

class ProfileHostInteractor(private val repository: IProfileHostRepository) {

    fun isUserLogged(): Boolean {
        return repository.isUserLogged()
    }
}