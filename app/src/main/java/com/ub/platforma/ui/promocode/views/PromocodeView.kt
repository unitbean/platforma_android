package com.ub.platforma.ui.promocode.views

import com.ub.platforma.ui.base.views.BaseView
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(OneExecutionStateStrategy::class)
interface PromocodeView : BaseView {
    fun onShowProgress(isShow: Boolean)
    fun onPromocodeResult(isAccepted: Boolean)
    fun onShowActivateError(errorText: String)
}