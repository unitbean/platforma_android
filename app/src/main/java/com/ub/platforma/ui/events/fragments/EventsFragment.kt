package com.ub.platforma.ui.events.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.doOnPreDraw
import com.platforma.repository.models.response.EventResponseItem
import com.ub.platforma.R
import com.ub.platforma.ui.event.fragments.EventFragment
import com.ub.platforma.ui.events.adapters.EventsRVAdapter
import com.ub.platforma.ui.events.interfaces.TransitionClickListener
import com.ub.platforma.ui.events.models.EventViewModel
import com.ub.platforma.ui.events.models.IEventViewModel
import com.ub.platforma.ui.events.presenters.EventsPresenter
import com.ub.platforma.ui.events.views.EventsView
import com.ub.platforma.ui.main.activities.MainActivity
import com.ub.platforma.ui.main.listeners.BackStackChangeListener
import com.ub.platforma.ui.subscription.fragments.SubscriptionFragment
import com.ub.platforma.ui.web.fragments.WebFragment
import com.ub.platforma.ui.web.interfaces.WebOperation
import com.ub.platforma.utils.ScrollAware
import com.ub.utils.gone
import com.ub.utils.visible
import kotlinx.android.synthetic.main.fragment_events.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter

class EventsFragment : MvpAppCompatFragment(), EventsView, TransitionClickListener, ScrollAware {

    @InjectPresenter lateinit var presenter: EventsPresenter

    private val adapter: EventsRVAdapter by lazy {
        EventsRVAdapter(false, presenter.isPremiumUser).apply {
            this.transitionClickListener = this@EventsFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_events, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rv_events_list.setHasFixedSize(true)
        rv_events_list.adapter = adapter
        srl_event_updater.setOnRefreshListener {
            presenter.loadEvents()
        }

        postponeEnterTransition()

        (view.parent as? ViewGroup)?.doOnPreDraw {
            startPostponedEnterTransition()
            (activity as? BackStackChangeListener)?.onBackStackChange(this)
        }

        presenter.loadEvents()
    }

    override fun onEventsLoaded(events: List<IEventViewModel>) {
        srl_event_updater.isRefreshing = false
        if (presenter.events.isNotEmpty()) {
            tv_empty_text.text = getString(R.string.events_empty_list)
            cl_events_empty.gone
            rv_events_list.visible
            srl_event_updater.isRefreshing = false
            adapter.update(events)
        } else {
            cl_events_empty.visible
            rv_events_list.gone
        }
    }

    override fun onShowError(message: String?) {
        srl_event_updater.isRefreshing = false
        if (!message.isNullOrEmpty()) {
            tv_empty_text.text = message
        } else {
            tv_empty_text.text = getString(R.string.error_common)
        }
    }

    override fun onItemClick(view: View, position: Int, transitions: Map<String, View>?) {
        when (view.id) {
            R.id.cl_events_container -> {
                val model = presenter.events[position] as EventViewModel
                if (model.allowContent) {
                    if (model.eventType == EventResponseItem.EventType.EVENT) {
                        (activity as? MainActivity)?.openScreen(EventFragment.TAG, Bundle().apply {
                            putParcelable(EVENT_MODEL, model)
                        }, sharedElements = transitions)
                    } else if (model.eventType == EventResponseItem.EventType.LONG_READ) {
                        (activity as? MainActivity)?.openScreen(WebFragment.TAG, Bundle().apply {
                            putParcelable(EVENT_MODEL, model)
                            putString(WebFragment.URL_OPERATION, WebOperation.READ_LONGREAD)
                        }, sharedElements = transitions)
                    }
                } else {
                    (activity as? MainActivity)?.openScreen(SubscriptionFragment.TAG, sharedElements = transitions)
                }
            }
            R.id.b_events_ads_button -> (activity as? MainActivity)?.openScreen(SubscriptionFragment.TAG)
        }
    }

    override fun onScrollOnTop() {
        rv_events_list.smoothScrollToPosition(0)
    }

    companion object {
        const val TAG = "EventsFragment"
        const val EVENT_MODEL = "event_model"

        fun newInstance(): EventsFragment {
            return EventsFragment()
        }
    }
}