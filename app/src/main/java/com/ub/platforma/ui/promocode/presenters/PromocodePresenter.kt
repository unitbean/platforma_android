package com.ub.platforma.ui.promocode.presenters

import com.ub.platforma.R
import com.ub.platforma.di.DIManager
import com.ub.platforma.ui.base.presenters.BasePresenter
import com.ub.platforma.ui.promocode.exceptions.PromocodeIsNullOrBlank
import com.ub.platforma.ui.promocode.interactors.PromocodeInteractor
import com.ub.platforma.ui.promocode.views.PromocodeView
import com.ub.utils.LogUtils
import kotlinx.coroutines.launch
import moxy.InjectViewState
import org.json.JSONObject
import retrofit2.HttpException
import javax.inject.Inject

@InjectViewState
class PromocodePresenter : BasePresenter<PromocodeView>() {

    @Inject lateinit var interactor: PromocodeInteractor

    var promocode: String? = null

    init {
        DIManager.getPromocodeSubcomponent().inject(this)
    }

    override fun onDestroy() {
        DIManager.clearPromocodeSubcomponent()
    }

    fun checkCode() {
        launch {
            try {
                viewState.onShowProgress(true)

                val isPremiumIsEmpty = interactor.checkPromocodeAndSave(promocode).endPremiumDate.isNullOrBlank()

                viewState.onPromocodeResult(!isPremiumIsEmpty)
            } catch (e: Exception) {
                if (e is HttpException) {
                    if (e.code() == 400) {
                        try {
                            val response = JSONObject(e.response()?.errorBody()?.string())
                            when (val message = response.getString("message")) {
                                "promoCode.notExist" -> viewState.onShowActivateError(DIManager.appComponent.context.getString(R.string.promocode_fragment_error_not_valid))
                                else -> viewState.onShowActivateError(message)
                            }

                        } catch (e: Exception) {
                            LogUtils.e(TAG, e.message, e)
                        }
                    } else if (e.code() == 401) {
                        viewState.onShowActivateError(DIManager.appComponent.context.getString(R.string.promocode_fragment_error_not_auth))
                    }
                } else if (e is PromocodeIsNullOrBlank) {
                    viewState.onShowActivateError(DIManager.appComponent.context.getString(R.string.promocode_fragment_error_is_null_or_empty))
                }
            } finally {
                viewState.onShowProgress(false)
            }
        }
    }

    companion object {
        private const val TAG = "PromocodePresenter"
    }
}