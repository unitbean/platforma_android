package com.ub.platforma.ui.subscription.views

import com.platforma.repository.models.PremiumStatus
import com.platforma.repository.models.response.BuyLinkResponse
import com.ub.platforma.ui.base.views.BaseView
import com.ub.platforma.ui.subscription.models.ISubscription
import com.ub.platforma.ui.subscription.models.SubscriptionViewModel
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(OneExecutionStateStrategy::class)
interface SubscriptionView : BaseView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun onSubscriptionUpdated(@PremiumStatus status: Int)

    fun onAvailableSubscriptionsLoaded(models: List<ISubscription>)
    fun onSignInCheckReady(isSignIn: Boolean, subscription: SubscriptionViewModel, buyLink: BuyLinkResponse?)
    fun onPromocodeAvailable(isPromocodeAvailable: Boolean)
}