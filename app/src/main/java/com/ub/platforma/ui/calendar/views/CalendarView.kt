package com.ub.platforma.ui.calendar.views

import com.ub.platforma.ui.base.views.BaseView
import com.ub.platforma.ui.events.models.EventViewModel
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(OneExecutionStateStrategy::class)
interface CalendarView : BaseView {
    fun eventsUpdated(events: List<EventViewModel>)
    fun onShowError(message: String?)
}