package com.ub.platforma.ui.profile.presenters

import com.ub.platforma.ui.profile.interactors.ProfileInteractor
import com.ub.platforma.di.DIManager
import com.ub.platforma.ui.base.presenters.BasePresenter
import com.ub.platforma.ui.profile.views.ProfileView
import javax.inject.Inject
import com.ub.utils.LogUtils
import kotlinx.coroutines.launch
import moxy.InjectViewState

@InjectViewState
class ProfilePresenter : BasePresenter<ProfileView>() {

    @Inject lateinit var interactor: ProfileInteractor

    val trialRemainsDays: Long
        get() = interactor.trialRemainingDays

    val endPremiumDate: String
        get() = interactor.endPremiumDate

    init {
        DIManager.getProfileSubcomponent().inject(this)
    }

    override fun onDestroy() {
        DIManager.clearProfileSubcomponent()
    }

    fun loadProfile() {
        launch {
            try {
                val user = interactor.loadProfile()
                viewState.onProfileLoaded(user)
            } catch (e: Exception) {
                LogUtils.e(TAG, e.message, e)
            }
        }
    }

    fun logout() {
        interactor.logout()
        viewState.onLogout()
    }

    companion object {
        private const val TAG = "ProfilePresenter"
    }
}