package com.ub.platforma.ui.signin.interfaces

import androidx.annotation.StringDef

@StringDef(SignInRedirectType.SUBSCRIPTION, SignInRedirectType.PROMOCODE)
@Retention(AnnotationRetention.SOURCE)
annotation class SignInRedirectType {
    companion object {
        const val SUBSCRIPTION = "subscription"
        const val PROMOCODE = "promocode"
    }
}