package com.ub.platforma.ui.partners.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.Fragment
import com.ub.platforma.R
import com.ub.platforma.ui.main.activities.MainActivity
import com.ub.platforma.ui.main.listeners.BackStackChangeListener
import com.ub.platforma.ui.partners.adapters.PartnersVPAdapter
import com.ub.platforma.utils.ScrollAware
import kotlinx.android.synthetic.main.component_toolbar.*
import kotlinx.android.synthetic.main.fragment_partners.*

class PartnersFragment : Fragment(), ScrollAware {

    private val vpAdapter: PartnersVPAdapter by lazy { PartnersVPAdapter(requireContext(), childFragmentManager) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_partners, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as? MainActivity)?.setSupportActionBar(toolbar)
        (activity as? MainActivity)?.supportActionBar?.setTitle(R.string.partner_title)

        postponeEnterTransition()

        (view.parent as? ViewGroup)?.doOnPreDraw {
            startPostponedEnterTransition()
            (activity as? BackStackChangeListener)?.onBackStackChange(this)
        }

        tl_partners_tabs.setupWithViewPager(vp_partners_content)
        vp_partners_content.adapter = vpAdapter
    }

    override fun onScrollOnTop() {
        vp_partners_content.setCurrentItem(0, true)
    }

    companion object {
        const val TAG = "PartnersFragment"
        const val PARTNER_MODEL = "partner_model"

        fun newInstance(): PartnersFragment {
            return PartnersFragment()
        }
    }
}