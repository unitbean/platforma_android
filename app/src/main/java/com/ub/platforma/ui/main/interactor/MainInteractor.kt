package com.ub.platforma.ui.main.interactor

import com.vk.api.sdk.auth.VKAccessToken
import com.facebook.login.LoginResult
import com.platforma.repository.main.IMainRepository
import com.platforma.repository.models.UserTokenModel
import com.platforma.repository.models.response.BuyLinkResponse
import com.ub.platforma.ui.main.exceptions.SubscriptionIdIsEmptyException

class MainInteractor(private val repository: IMainRepository) {

    /**
     * Попытка логина через ВКонтакте
     * @param res - данные о входе через ВК
     */
    suspend fun onVkSignIn(res: VKAccessToken): Boolean {
        repository.loginWithVk(res)
        repository.logLoginEventWithType(UserTokenModel.TokenType.VK)

        return repository.isAuthSuccess()
    }

    /**
     * Попытка логина через FaceBook
     * @param res - данные о входе через FB
     */
    suspend fun onFbSignIn(res: LoginResult): Boolean {
        repository.loginWithFb(res)
        repository.logLoginEventWithType(UserTokenModel.TokenType.FB)

        return repository.isAuthSuccess()
    }

    fun logEvent(key: String) {
        repository.logEvent(key)
    }

    /**
     * Загружает сохраненного ранее пользователя
     */
    suspend fun loadSavedUser() {
        repository.loadEvents()
        val token = repository.restoreUserToken()
        if (token != null) {
            val user = when (token.type) {
                UserTokenModel.TokenType.FB -> repository.loadFbUser()
                UserTokenModel.TokenType.VK -> repository.loadVkUser()
                else -> repository.loadVkUser()
            }

            repository.saveUser(user)
        }
    }

    fun isAuth(): Boolean = repository.isAuthSuccess()

    suspend fun getBuyLink(subscriptionId: String?): BuyLinkResponse {
        if (subscriptionId.isNullOrEmpty()) throw SubscriptionIdIsEmptyException()
        return repository.receiveBuyLink(subscriptionId)
    }
}