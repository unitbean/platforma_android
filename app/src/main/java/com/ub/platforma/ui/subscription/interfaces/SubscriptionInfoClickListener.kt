package com.ub.platforma.ui.subscription.interfaces

import com.platforma.repository.models.response.BuyLinkResponse
import com.ub.platforma.ui.subscription.models.SubscriptionViewModel

interface SubscriptionInfoClickListener {

    fun onAuthorize(subscription: SubscriptionViewModel, buyLink: BuyLinkResponse?)
    fun showPromocode()
}