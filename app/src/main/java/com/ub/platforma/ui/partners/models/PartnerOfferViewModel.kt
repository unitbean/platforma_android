package com.ub.platforma.ui.partners.models

import com.platforma.repository.models.response.PartnerOfferResponseItem
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PartnerOfferViewModel(val partnerId: String,
                                 val partnerTitle: String,
                                 val partnerDescription: String,
                                 val partnerPreviewText: String?,
                                 val partnerPreviewPic: String?,
                                 val partnerMainPic: String?,
                                 val partnerLogo: String?,
                                 val promoCode: String?,
                                 val partnerAllow: Boolean): BaseOfferModel(
    partnerId,
    partnerTitle,
    partnerDescription,
    partnerPreviewText,
    partnerPreviewPic,
    partnerMainPic,
    partnerLogo,
    partnerAllow
) {

    constructor(model: PartnerOfferResponseItem) : this (
        model.id,
        model.title,
        model.description,
        model.previewText,
        model.previewPic,
        model.mainPic,
        model.logo,
        model.promoCode,
        model.allow
    )

    override fun getItemId(): Int = id.hashCode()
}