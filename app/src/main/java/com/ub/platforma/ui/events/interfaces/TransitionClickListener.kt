package com.ub.platforma.ui.events.interfaces

import android.view.View

interface TransitionClickListener {
    fun onItemClick(view: View, position: Int, transitions: Map<String, View>?)
}