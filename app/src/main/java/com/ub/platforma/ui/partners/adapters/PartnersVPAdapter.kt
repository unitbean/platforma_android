package com.ub.platforma.ui.partners.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.ub.platforma.R
import com.ub.platforma.ui.partners.fragments.PartnerItemFragment

class PartnersVPAdapter(private val context: Context, manager: FragmentManager) : FragmentPagerAdapter(manager) {

    private val partnersFragment: PartnerItemFragment by lazy { PartnerItemFragment.newInstance(false) }
    private val ticketsFragment: PartnerItemFragment by lazy { PartnerItemFragment.newInstance(true) }

    override fun getItem(position: Int): Fragment {
        return when(position) {
            0 -> partnersFragment
            1 -> ticketsFragment
            else -> partnersFragment
        }
    }

    override fun getCount(): Int = 2

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> context.getString(R.string.partner_page_discounts_title)
            1 -> context.getString(R.string.partner_page_tickets_title)
            else -> context.getString(R.string.partner_page_discounts_title)
        }
    }
}