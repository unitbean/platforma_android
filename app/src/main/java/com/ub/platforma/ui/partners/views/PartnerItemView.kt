package com.ub.platforma.ui.partners.views

import com.ub.platforma.ui.base.views.BaseView
import com.ub.platforma.ui.partners.models.BaseOfferModel
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(OneExecutionStateStrategy::class)
interface PartnerItemView : BaseView {
    fun onPartnersLoad(partners: List<BaseOfferModel>)
    fun onShowError(message: String?)
}