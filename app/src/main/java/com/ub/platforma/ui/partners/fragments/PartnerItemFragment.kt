package com.ub.platforma.ui.partners.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ub.platforma.R
import com.ub.platforma.ui.events.interfaces.TransitionClickListener
import com.ub.platforma.ui.main.activities.MainActivity
import com.ub.platforma.ui.partner.fragments.PartnerFragment
import com.ub.platforma.ui.partners.adapters.PartnersRVAdapter
import com.ub.platforma.ui.partners.models.BaseOfferModel
import com.ub.platforma.ui.partners.presenters.PartnerItemPresenter
import com.ub.platforma.ui.partners.views.PartnerItemView
import com.ub.platforma.ui.subscription.fragments.SubscriptionFragment
import com.ub.utils.argument
import com.ub.utils.gone
import com.ub.utils.visible
import kotlinx.android.synthetic.main.fragment_partner_item.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter

class PartnerItemFragment : MvpAppCompatFragment(), PartnerItemView, TransitionClickListener {

    private val isInTicketMode: Boolean by argument(TICKET_MODE)

    @InjectPresenter
    lateinit var presenter: PartnerItemPresenter

    private val adapter: PartnersRVAdapter by lazy {
        PartnersRVAdapter().apply {
            this.transitionClickListener = this@PartnerItemFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_partner_item, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rv_partners_data.setHasFixedSize(true)
        rv_partners_data.adapter = adapter
        srl_partners_data.setOnRefreshListener {
            presenter.loadOffers(isInTicketMode)
        }

        if (presenter.offers.isNotEmpty()) {
            rv_partners_data.visible
            cl_partners_empty.gone
        }

        tv_empty_text.text = if (isInTicketMode) {
            getString(R.string.tickets_empty_list)
        } else {
            getString(R.string.partners_empty_list)
        }
    }

    override fun onResume() {
        super.onResume()

        presenter.loadOffers(isInTicketMode)
    }

    override fun onPartnersLoad(partners: List<BaseOfferModel>) {
        srl_partners_data.isRefreshing = false
        if (presenter.offers.isNotEmpty()) {
            rv_partners_data.visible
            cl_partners_empty.gone
            adapter.update(partners)
        } else {
            rv_partners_data.gone
            cl_partners_empty.visible
        }
    }

    override fun onShowError(message: String?) {
        srl_partners_data.isRefreshing = false
        if (!message.isNullOrEmpty()) {
            tv_empty_text.text = message
        } else {
            tv_empty_text.text = getString(R.string.error_common)
        }
    }

    override fun onItemClick(view: View, position: Int, transitions: Map<String, View>?) {
        when (view.id) {
            R.id.cl_partners_container -> openPartnerOrNot(position, transitions)
        }
    }

    private fun openPartnerOrNot(position: Int, transitions: Map<String, View>?) {
        if (presenter.offers[position].allow == true) {
            (requireActivity() as MainActivity).openScreen(PartnerFragment.TAG, Bundle().apply {
                putParcelable(PartnersFragment.PARTNER_MODEL, presenter.offers[position])
            }, sharedElements = transitions)
        } else {
            (requireActivity() as MainActivity).openScreen(SubscriptionFragment.TAG, Bundle())
        }
    }

    companion object {
        const val TAG = "PartnersItemFragment"

        private const val TICKET_MODE = "is_ticket_mode"

        fun newInstance(isInTicketMode: Boolean): PartnerItemFragment {
            return PartnerItemFragment().apply {
                this.arguments = Bundle().apply {
                    putBoolean(TICKET_MODE, isInTicketMode)
                }
            }
        }
    }
}