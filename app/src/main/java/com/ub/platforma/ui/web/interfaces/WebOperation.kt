package com.ub.platforma.ui.web.interfaces

import androidx.annotation.StringDef

@StringDef(
    WebOperation.EVENT_REGISTER,
    WebOperation.BUY_SUBSCRIPTION,
    WebOperation.MANAGE_SUBSCRIPTION,
    WebOperation.READ_LONGREAD
)
@Retention(AnnotationRetention.SOURCE)
annotation class WebOperation {
    companion object {
        const val EVENT_REGISTER = "event_register"
        const val BUY_SUBSCRIPTION = "buy_subscription"
        const val MANAGE_SUBSCRIPTION = "manage_subscription"
        const val READ_LONGREAD = "read_longread"
    }
}