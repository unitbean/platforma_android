package com.ub.platforma.ui.partner.presenters

import com.ub.platforma.R
import com.ub.platforma.di.DIManager
import com.ub.platforma.ui.base.presenters.BasePresenter
import com.ub.platforma.ui.partner.exceptions.*
import com.ub.platforma.ui.partner.interactors.TicketGetInteractor
import com.ub.platforma.ui.partner.views.TicketGetView
import com.ub.platforma.ui.partners.models.TicketOfferViewModel
import kotlinx.coroutines.launch
import moxy.InjectViewState
import retrofit2.HttpException
import javax.inject.Inject

@InjectViewState
class TicketGetPresenter : BasePresenter<TicketGetView>() {

    @Inject lateinit var interactor: TicketGetInteractor

    init {
        DIManager.getTicketGetSubcomponent().inject(this)
    }

    override fun onDestroy() {
        DIManager.clearTicketGetSubcomponent()
    }

    fun validateAndBookTicket(name: String, phone: String, ticket: TicketOfferViewModel) {
        launch {
            try {
                interactor.validateAndBookTicket(name, phone, ticket)

                viewState.onTicketBooked()
            } catch (e: Exception) {
                when (e) {
                    is NameAndPhoneRequiredException -> viewState.onShowError(R.string.ticket_get_error_name_and_phone)
                    is NameRequiredException -> viewState.onShowError(R.string.ticket_get_error_name)
                    is PhoneRequiredException -> viewState.onShowError(R.string.ticket_get_error_phone)
                    is HttpException -> viewState.onShowError(R.string.error_common)
                    else -> viewState.onShowError(R.string.ticket_get_error_common)
                }
            }
        }
    }
}