package com.ub.platforma.ui.partner.fragments

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.transition.TransitionInflater
import coil.api.load
import com.ub.platforma.R
import com.ub.platforma.ui.main.activities.MainActivity
import com.ub.platforma.ui.main.listeners.BackStackChangeListener
import com.ub.platforma.ui.partner.dialogs.TicketGetDialog
import com.ub.platforma.ui.partner.presenters.PartnerPresenter
import com.ub.platforma.ui.partner.views.PartnerView
import com.ub.platforma.ui.partners.fragments.PartnersFragment
import com.ub.platforma.ui.partners.models.BaseOfferModel
import com.ub.platforma.ui.partners.models.PartnerOfferViewModel
import com.ub.platforma.ui.partners.models.TicketOfferViewModel
import com.ub.platforma.ui.web.fragments.toPicUrl
import com.ub.platforma.ui.web.fragments.toPictureUrlScaled
import com.ub.platforma.utils.CircleTransform
import com.ub.utils.UbUtils
import com.ub.utils.gone
import com.ub.utils.visible
import kotlinx.android.synthetic.main.fragment_partner.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter

class PartnerFragment : MvpAppCompatFragment(), PartnerView, View.OnClickListener {

    @InjectPresenter lateinit var presenter: PartnerPresenter

    private var partner: BaseOfferModel? = null
    private var ticketGetDialog: TicketGetDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_partner, container, false)

        partner = arguments?.getParcelable(PartnersFragment.PARTNER_MODEL)

        view.findViewById<View>(R.id.iv_partner_image).transitionName = "image-${partner?.id}"
        view.findViewById<View>(R.id.v_partner_gradient).transitionName = "gradient-${partner?.id}"
        view.findViewById<View>(R.id.iv_partner_sponsor_logo).transitionName = "logo-${partner?.id}"
        view.findViewById<View>(R.id.tv_partner_sponsor_title).transitionName = "title-${partner?.id}"
        view.findViewById<View>(R.id.tv_partner_sponsor_short_description).transitionName = "description-${partner?.id}"
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        postponeEnterTransition()

        iv_partner_image.load((partner?.mainPic ?: "").toPicUrl()) {
            listener { _, _ ->
                startPostponedEnterTransition()
                (requireActivity() as MainActivity).showNavigationBar(false)
                (activity as? BackStackChangeListener)?.onBackStackChange(this@PartnerFragment)
            }
        }

        iv_partner_sponsor_logo.load((partner?.logo ?: "").toPictureUrlScaled()) {
            transformations(CircleTransform(2f, Color.WHITE))
        }

        tv_partner_sponsor_title.text = partner?.title
        tv_partner_sponsor_short_description.text = partner?.previewText
        tv_partner_description.text = partner?.description

        (partner as? PartnerOfferViewModel)?.let { offer ->
            if (offer.promoCode.isNullOrEmpty()) {
                tv_partner_code.gone
            } else {
                tv_partner_code.visible
                tv_partner_code.text = offer.promoCode
                tv_partner_code.setOnClickListener {
                    UbUtils.copyTextToClipboard(tv_partner_code.text.toString())
                }
            }
        } ?: (partner as? TicketOfferViewModel)?.let { ticket ->
            tv_partner_code.setText(R.string.partner_detail_page_ticket_get)
            tv_partner_code.setOnClickListener {
                if (ticketGetDialog?.isAdded != true) {
                    ticketGetDialog = TicketGetDialog.newInstance().apply {
                        arguments = Bundle().apply {
                            putParcelable(TicketGetDialog.TICKET_EXTRA, ticket)
                        }
                        show(this@PartnerFragment.childFragmentManager, TicketGetDialog.TAG)
                    }
                }
            }
        }
        b_partner_back.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.b_partner_back -> requireActivity().onBackPressed()
        }
    }

    companion object {
        const val TAG = "PartnerFragment"

        fun newInstance(extras: Bundle?): PartnerFragment {
            return PartnerFragment().apply {
                arguments = extras
            }
        }
    }
}