package com.ub.platforma.ui.profilehost.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ub.platforma.R
import com.ub.platforma.ui.main.listeners.BackStackChangeListener
import com.ub.platforma.ui.profile.fragments.ProfileFragment
import com.ub.platforma.ui.profilehost.presenters.ProfileHostPresenter
import com.ub.platforma.ui.profilehost.views.ProfileHostView
import com.ub.platforma.ui.signin.fragments.SignInFragment
import com.ub.platforma.utils.FragmentNavigation
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter

class ProfileHostFragment : MvpAppCompatFragment(), ProfileHostView, FragmentNavigation {

    @InjectPresenter lateinit var presenter: ProfileHostPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile_host, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        updateFragment()

        (activity as? BackStackChangeListener)?.onBackStackChange(this)
    }

    fun updateFragment() {
        val isSignIn = presenter.isShowSignIn()
        val fragment: Fragment = if (isSignIn) {
            SignInFragment.newInstance()
        } else {
            ProfileFragment.newInstance()
        }

        val tag: String = if (isSignIn) {
            SignInFragment.TAG
        } else {
            ProfileFragment.TAG
        }

        if (childFragmentManager.primaryNavigationFragment?.tag == tag) return
        childFragmentManager.attachFragment(R.id.fl_profile_host_container, fragment, tag)
    }

    companion object {
        const val TAG = "ProfileHostFragment"

        fun newInstance(): ProfileHostFragment {
            return ProfileHostFragment()
        }
    }
}