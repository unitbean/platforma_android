package com.ub.platforma.ui.event.views

import com.ub.platforma.ui.base.views.BaseView
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(OneExecutionStateStrategy::class)
interface EventView : BaseView {
    fun setParticipantStatus(isParticipant: Boolean)
    fun onParticipantChanged(isParticipant: Boolean)
}