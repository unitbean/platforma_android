package com.ub.platforma.ui.signin.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.ub.platforma.R
import com.ub.platforma.ui.main.activities.MainActivity
import com.ub.utils.gone
import com.ub.utils.visible
import com.vk.api.sdk.VK
import kotlinx.android.synthetic.main.component_toolbar.*
import kotlinx.android.synthetic.main.fragment_sign_in.*
import android.content.Intent
import android.net.Uri

class SignInFragment : Fragment(), View.OnClickListener {

    private var isEmbeddedMode: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.title = getString(R.string.sign_in_title)

        isEmbeddedMode = arguments?.getBoolean(IS_EMBEDDED_MODE) ?: false

        if (isEmbeddedMode) {
            apl_sign_in_toolbar.gone
            b_restore_purchases.gone
        } else {
            apl_sign_in_toolbar.visible
            b_restore_purchases.visible
        }

        b_vk_login.setOnClickListener(this)
        b_facebook_login.setOnClickListener(this)
        b_restore_purchases.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.b_vk_login -> VK.login(requireActivity())
            R.id.b_facebook_login -> (requireActivity() as MainActivity).loginFacebook()
            R.id.b_restore_purchases -> createRestorePurchasesAlert()?.show()
        }
    }

    fun showProgress(isShow: Boolean) {
        if (isShow) {
            b_vk_login.gone
            b_facebook_login.gone
            tv_empty_text.gone

            if (!isEmbeddedMode) {
                b_restore_purchases.gone
            }
            pb_sign_in_progress.visible
        } else {
            b_vk_login.visible
            b_facebook_login.visible
            tv_empty_text.visible

            if (!isEmbeddedMode) {
                b_restore_purchases.visible
            }
            pb_sign_in_progress.gone
        }
    }

    companion object {
        const val TAG = "SignInFragment"

        const val IS_EMBEDDED_MODE = "is_embedded_mode"
        const val REDIRECT_AFTER = "redirect_after"
        const val SELECTED_ID = "selected_id"

        fun newInstance(extras: Bundle? = null): SignInFragment {
            return SignInFragment().apply {
                arguments = extras
            }
        }
    }
}

fun Fragment.createRestorePurchasesAlert(): AlertDialog? {
    return AlertDialog.Builder(context ?: return null)
        .setTitle(R.string.sign_restore_purchases)
        .setMessage(R.string.sign_in_restore_purchases)
        .setPositiveButton(R.string.sign_in_restore_purchases_open_group) { _, _ ->
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("https://vk.com/platforma_34")
            if (context?.packageManager?.resolveActivity(intent, 0) != null) {
                startActivity(intent)
            }
        }
        .create()
}