package com.ub.platforma.ui.subscription.interfaces

import com.ub.platforma.ui.subscription.models.ISubscription

interface SubscriptionClickListener {
    fun onSubscriptionClick(subscription: ISubscription)
}