package com.ub.platforma.ui.events.interactors

import com.ub.platforma.ui.events.models.AdsViewModel
import com.ub.platforma.ui.events.models.EventViewModel
import com.ub.platforma.ui.events.models.HeaderViewModel
import com.ub.platforma.ui.events.models.IEventViewModel
import com.platforma.repository.events.IEventsRepository

class EventsInteractor(private val repository: IEventsRepository) {

    suspend fun loadEvents(): List<IEventViewModel> {
        val events: MutableList<IEventViewModel> = repository.loadEvents().result.map {
            EventViewModel(it)
        }.toMutableList()

        if (events.isNotEmpty()) {
            events.add(0, HeaderViewModel(""))
        }

        if (events.size >= 5 && !repository.isPremiumUser()) {
            events.add(4, AdsViewModel("ads4"))
        }

        return events
    }

    fun isPremiumUser(): Boolean {
        return repository.isPremiumUser()
    }
}