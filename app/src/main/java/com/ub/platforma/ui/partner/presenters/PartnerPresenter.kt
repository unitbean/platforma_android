package com.ub.platforma.ui.partner.presenters

import com.ub.platforma.di.DIManager
import com.ub.platforma.ui.base.presenters.BasePresenter
import com.ub.platforma.ui.partner.interactors.PartnerInteractor
import com.ub.platforma.ui.partner.views.PartnerView
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class PartnerPresenter : BasePresenter<PartnerView>() {

    @Inject lateinit var interactor: PartnerInteractor

    init {
        DIManager.getPartnerSubcomponent().inject(this)
    }

    override fun onDestroy() {
        DIManager.clearPartnerSubcomponent()
    }
}