package com.ub.platforma.ui.subscription.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import coil.ImageLoader
import coil.api.load
import coil.decode.GifDecoder
import com.ub.platforma.R
import com.ub.platforma.ui.web.fragments.WebFragment
import com.ub.platforma.utils.BackPressedAware
import com.ub.utils.dpToPx
import kotlinx.android.synthetic.main.fragment_subscription_final.*

class SubscriptionFinalFragment : Fragment(), View.OnClickListener, BackPressedAware {

    private var isFinalSuccess: Boolean? = null
    private var sourceScreenTag: String? = null
    private val gifLoader: ImageLoader by lazy {
        ImageLoader(requireContext()) {
            componentRegistry {
                add(GifDecoder())
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_subscription_final, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        isFinalSuccess = arguments?.getBoolean(IS_SUCCESS, false)
        sourceScreenTag = arguments?.getString(SOURCE_SCREEN, null)

        if (isFinalSuccess == true) {
            iv_promocode_final_image.load(R.raw.success, gifLoader)
            tv_promocode_final_message.text = getString(R.string.subscription_success_text)
            b_promocode_final_action.text = getString(R.string.subscription_success_action)
        } else {
            iv_promocode_final_image.load(R.drawable.ic_part_empty)
            tv_promocode_final_message.text = getString(R.string.subscription_fail_text)
            b_promocode_final_action.text = getString(R.string.subscription_fail_action)
        }

        b_promocode_final_action.setOnClickListener(this)
        iv_promocode_final_close.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.b_promocode_final_action -> activity?.supportFragmentManager?.popBackStack(sourceScreenTag ?: WebFragment.TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            R.id.iv_promocode_final_close -> activity?.supportFragmentManager?.popBackStack(sourceScreenTag ?: WebFragment.TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
    }

    override fun onBackPressed(): Boolean {
        activity?.supportFragmentManager?.popBackStack(sourceScreenTag ?: WebFragment.TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        return true
    }

    companion object {
        const val TAG = "SubscriptionFinalFragment"
        const val IS_SUCCESS = "is_success"
        const val SOURCE_SCREEN = "source_screen"

        fun newInstance(extras: Bundle?): SubscriptionFinalFragment {
            return SubscriptionFinalFragment().apply {
                arguments = extras
            }
        }
    }
}