package com.ub.platforma.ui.subscription.models

import android.os.Parcelable
import com.platforma.repository.models.response.SubscriptionResponse
import com.ub.utils.base.DiffComparable
import kotlinx.android.parcel.Parcelize

interface ISubscription : DiffComparable

data class HeaderViewModel(
    val header: String = "header"
) : ISubscription {
    override fun getItemId(): Int = header.hashCode()
}

data class PromocodeViewModel(
    val promoCode: String = "code"
) : ISubscription {
    override fun getItemId(): Int = promoCode.hashCode()
}

data class ManageViewModel(
    val manage: String = "manage"
) : ISubscription {
    override fun getItemId(): Int = manage.hashCode()
}

@Parcelize
data class SubscriptionViewModel(
    val id: String,
    val title: String,
    val article: String,
    val price: Int,
    val previousPrice: Int,
    val timeValue: Int,
    @SubscriptionResponse.SubscriptionPeriod
    val timeType: String
) : ISubscription, Parcelable {

    constructor(subscription: SubscriptionResponse): this(
        subscription.id,
        subscription.title,
        subscription.article,
        subscription.price,
        subscription.previousPrice,
        subscription.timeValue,
        subscription.timeType
    )

    override fun getItemId(): Int = id.hashCode()
}