package com.ub.platforma.ui.events.presenters

import com.ub.platforma.R
import com.ub.platforma.di.DIManager
import com.ub.platforma.ui.base.presenters.BasePresenter
import com.ub.platforma.ui.events.interactors.EventsInteractor
import com.ub.platforma.ui.events.models.IEventViewModel
import com.ub.platforma.ui.events.views.EventsView
import com.ub.utils.LogUtils
import com.ub.utils.renew
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moxy.InjectViewState
import java.io.IOException
import javax.inject.Inject

@InjectViewState
class EventsPresenter : BasePresenter<EventsView>() {

    @Inject lateinit var interactor: EventsInteractor

    val events = ArrayList<IEventViewModel>()
    val isPremiumUser: Boolean
        get() = interactor.isPremiumUser()

    init {
        DIManager.getEventsSubcomponent().inject(this)
    }

    override fun onDestroy() {
        DIManager.clearEventsSubcomponent()
    }

    fun loadEvents() {
        launch {
            try {
                val newEvents = withContext(Dispatchers.IO) { interactor.loadEvents() }
                events.renew(newEvents)
            } catch (e: Exception) {
                events.clear()
                LogUtils.e(TAG, e.message, e)
                if (e is IOException) {
                    viewState.onShowError(DIManager.appComponent.context.getString(R.string.error_network_connection))
                }
            }

            viewState.onEventsLoaded(events)
        }
    }

    companion object {
        private const val TAG = "EventsPresenter"
    }
}