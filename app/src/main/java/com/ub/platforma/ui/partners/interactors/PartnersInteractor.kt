package com.ub.platforma.ui.partners.interactors

import com.ub.platforma.ui.partners.models.BaseOfferModel
import com.ub.platforma.ui.partners.models.PartnerOfferViewModel
import com.ub.platforma.ui.partners.models.TicketOfferViewModel
import com.platforma.repository.partners.IPartnersRepository

class PartnersInteractor(private val repository: IPartnersRepository) {

    suspend fun loadPartners(): List<BaseOfferModel> {
        return repository.loadPartners().map { PartnerOfferViewModel(it) }
    }

    suspend fun loadTickets(): List<BaseOfferModel> {
        return repository.loadTickets().map { TicketOfferViewModel(it) }
    }
}