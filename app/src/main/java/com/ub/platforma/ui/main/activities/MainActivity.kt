package com.ub.platforma.ui.main.activities

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import androidx.annotation.ColorInt
import android.view.MenuItem
import android.view.View
import android.view.View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.platforma.repository.AnalyticsService
import com.platforma.repository.models.response.BuyLinkResponse
import com.google.android.material.snackbar.Snackbar
import com.ub.platforma.R
import com.ub.platforma.ui.calendar.fragments.CalendarFragment
import com.ub.platforma.ui.event.fragments.EventFragment
import com.ub.platforma.ui.events.fragments.EventsFragment
import com.ub.platforma.ui.main.commons.UpdateManager
import com.ub.platforma.ui.main.listeners.BackStackChangeListener
import com.ub.platforma.ui.partners.fragments.PartnersFragment
import com.ub.platforma.ui.main.presenters.MainPresenter
import com.ub.platforma.ui.main.views.MainView
import com.ub.platforma.ui.partner.fragments.PartnerFragment
import com.ub.platforma.ui.partners.models.PartnerOfferViewModel
import com.ub.platforma.ui.partners.models.TicketOfferViewModel
import com.ub.platforma.ui.profilehost.fragments.ProfileHostFragment
import com.ub.platforma.ui.promocode.fragments.PromocodeFragment
import com.ub.platforma.ui.signin.fragments.SignInFragment
import com.ub.platforma.ui.signin.interfaces.SignInRedirectType
import com.ub.platforma.ui.splash.fragments.SplashFragment
import com.ub.platforma.ui.subscription.fragments.SubscriptionFinalFragment
import com.ub.platforma.ui.subscription.fragments.SubscriptionFragment
import com.ub.platforma.ui.web.fragments.WebFragment
import com.ub.platforma.ui.web.interfaces.WebOperation
import com.ub.platforma.utils.BackPressedAware
import com.ub.platforma.utils.FragmentNavigation
import com.ub.platforma.utils.ScrollAware
import com.vk.api.sdk.VK
import com.vk.api.sdk.auth.VKAccessToken
import com.vk.api.sdk.auth.VKAuthCallback
import kotlinx.android.synthetic.main.activity_main.*
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter

class MainActivity : MvpAppCompatActivity(), MainView, FragmentNavigation, BackStackChangeListener {

    @InjectPresenter lateinit var presenter: MainPresenter

    private val callbackManager: CallbackManager by lazy { CallbackManager.Factory.create() }
    private val updateManager: UpdateManager by lazy { UpdateManager(this, cl_main_root) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bnv_main_navigation.setOnNavigationItemSelectedListener {  menuItem ->
            return@setOnNavigationItemSelectedListener when (menuItem.itemId) {
                R.id.menu_events -> openScreen(EventsFragment.TAG, isAddToBackStack = false)
                R.id.menu_calendar -> openScreen(CalendarFragment.TAG, isAddToBackStack = false)
                R.id.menu_partner -> openScreen(PartnersFragment.TAG, isAddToBackStack = false)
                R.id.menu_profile -> openScreen(ProfileHostFragment.TAG, isAddToBackStack = false)
                else -> false
            }
        }

        bnv_main_navigation.setOnNavigationItemReselectedListener {
            tapOnOpenedTab()
        }

        val savedVisibility = savedInstanceState?.getBoolean(NAVIGATION_IS_VISIBLE)
        savedVisibility?.let { showNavigationBar(it) }
        val savedColor = savedInstanceState?.getInt(STATUS_BAR_COLOR)
        val savedLight = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            savedInstanceState?.getBoolean(STATUS_BAR_IS_LIGHT)
        } else {
            false
        }
        if (savedColor != null && savedLight != null) {
            colorizeStatusBar(savedColor, savedLight)
        }

        updateManager.checkAndShow()
    }

    override fun onStop() {
        super.onStop()

        updateManager.detachListener()
    }

    override fun onStart() {
        super.onStart()

        updateManager.attachListener()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putInt(STATUS_BAR_COLOR, window.statusBarColor)
        outState.putBoolean(NAVIGATION_IS_VISIBLE, bnv_main_navigation.visibility == View.VISIBLE)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            outState.putBoolean(STATUS_BAR_IS_LIGHT, window.decorView.systemUiVisibility == View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            outState.putBoolean(STATUS_BAR_IS_LIGHT, window.decorView.systemUiVisibility == View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!VK.onActivityResult(requestCode, resultCode, data, object : VKAuthCallback {
                override fun onLogin(token: VKAccessToken) {
                    presenter.onVkSignIn(token)
                    (supportFragmentManager.primaryNavigationFragment as? ProfileHostFragment)?.let {
                        (it.childFragmentManager.primaryNavigationFragment as? SignInFragment)?.showProgress(true)
                    } ?: (supportFragmentManager.primaryNavigationFragment as? SignInFragment)?.showProgress(true)
                }

                override fun onLoginFailed(errorCode: Int) {
                    if (errorCode != VKAuthCallback.AUTH_CANCELED) {
                        Snackbar.make(fl_main_container, R.string.sign_in_error, Snackbar.LENGTH_SHORT).show()
                    }
                }
            })) {
            super.onActivityResult(requestCode, resultCode, data)
            callbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }

    override fun onBackStackChange(fragment: Fragment) {
        if (fragment is SubscriptionFragment) {
            colorizeStatusBar(Color.BLACK, false)
        } else {
            colorizeStatusBar(Color.WHITE, true)
        }

        when (fragment) {
            is EventsFragment -> {
                showNavigationBar(true)
                bnv_main_navigation.menu.findItem(R.id.menu_events).isChecked = true
            }
            is CalendarFragment -> {
                showNavigationBar(true)
                bnv_main_navigation.menu.findItem(R.id.menu_calendar).isChecked = true
            }
            is PartnersFragment -> {
                showNavigationBar(true)
                bnv_main_navigation.menu.findItem(R.id.menu_partner).isChecked = true
            }
            is ProfileHostFragment -> {
                showNavigationBar(true)
                bnv_main_navigation.menu.findItem(R.id.menu_profile).isChecked = true
            }
            is EventFragment -> showNavigationBar(false)
            is SubscriptionFragment -> showNavigationBar(false)
            is PromocodeFragment -> showNavigationBar(false)
            is WebFragment -> showNavigationBar(false)
        }
    }

    override fun onFirstOpen() {
        openScreen(SplashFragment.TAG, isAddToBackStack = false)
        showNavigationBar(false)
    }

    override fun onLoginSuccess(isAuth: Boolean) {
        (supportFragmentManager.primaryNavigationFragment as? ProfileHostFragment)?.let {
            (it.childFragmentManager.primaryNavigationFragment as? SignInFragment)?.showProgress(false)
            it.updateFragment()
        } ?: (supportFragmentManager.primaryNavigationFragment as? SignInFragment)?.let {
            it.showProgress(false)
            when (it.arguments?.getString(SignInFragment.REDIRECT_AFTER)) {
                SignInRedirectType.SUBSCRIPTION -> presenter.loadBuyLinkFromSubscriptionId(
                    it.arguments?.getString(SignInFragment.SELECTED_ID),
                    it.arguments?.getString(AnalyticsService.FB_CONTENT_TYPE),
                    it.arguments?.getString(AnalyticsService.FB_CONTENT_DATA),
                    it.arguments?.getInt(AnalyticsService.FB_CONTENT_PRICE))
                SignInRedirectType.PROMOCODE -> openScreen(PromocodeFragment.TAG)
                else -> supportFragmentManager.popBackStack(SignInFragment.TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            }
        }
    }

    override fun onBuyLinkReady(buyLinkResponse: BuyLinkResponse, id: String, type: String, data: String, price: Int) {
        openScreen(WebFragment.TAG, Bundle().apply {
            putString(WebFragment.URL_TO_OPEN, buyLinkResponse.orderId)
            putString(WebFragment.URL_TO_SUCCESS, buyLinkResponse.successUrl)
            putString(WebFragment.URL_TO_FAIL, buyLinkResponse.errorUrl)
            putString(WebFragment.URL_OPERATION, WebOperation.BUY_SUBSCRIPTION)
            putString(AnalyticsService.FB_CONTENT_DATA, data)
            putString(AnalyticsService.FB_CONTENT_TYPE, type)
            putString(AnalyticsService.FB_CONTENT_ID, id)
            putInt(AnalyticsService.FB_CONTENT_PRICE, price)
            putString(SubscriptionFinalFragment.SOURCE_SCREEN, SignInFragment.TAG)
        })
    }

    override fun onShowError(message: String?) {
        (supportFragmentManager.primaryNavigationFragment as? ProfileHostFragment)?.let {
            (it.childFragmentManager.primaryNavigationFragment as? SignInFragment)?.showProgress(false)
        } ?: (supportFragmentManager.primaryNavigationFragment as? SignInFragment)?.showProgress(false)
        Snackbar.make(fl_main_container, message ?: getString(R.string.sign_in_error), Snackbar.LENGTH_SHORT).show()
    }

    override fun onBackPressed() {
        when {
            (supportFragmentManager.primaryNavigationFragment as? BackPressedAware) != null -> {
                if ((supportFragmentManager.primaryNavigationFragment as? BackPressedAware)?.onBackPressed() != true) {
                    when {
                        supportFragmentManager.backStackEntryCount != 0 -> super.onBackPressed()
                        presenter.fragmentStack.size > 1 -> {
                            presenter.fragmentStack.pop()
                            bnv_main_navigation.selectedItemId = presenter.fragmentStack.lastElement()
                        }
                        else -> super.onBackPressed()
                    }
                }
            }
            supportFragmentManager.backStackEntryCount != 0 -> super.onBackPressed()
            presenter.fragmentStack.size > 1 -> {
                presenter.fragmentStack.pop()
                bnv_main_navigation.selectedItemId = presenter.fragmentStack.lastElement()
            }
            else -> super.onBackPressed()
        }
    }

    /**
     * Функция навигации, переключающая фрагменты экранов
     * @param tag - имя фрагмента
     * @param extras - дополнительные параметры для открытия фрагмента. может быть null
     * @param isAddToBackStack - добавлять ли транзакцию в очередь. по-умолчанию true
     * @param sharedElements - набор для анимации переходов между экранами
     */
    fun openScreen(tag: String, extras: Bundle? = null, isAddToBackStack: Boolean = true, sharedElements: Map<String, View>? = null): Boolean {
        return when (tag) {
            EventsFragment.TAG -> {
                presenter.logEvent(AnalyticsService.SCR_OPEN_FEED)
                presenter.addFragmentToStack(R.id.menu_events)
                colorizeStatusBar(Color.WHITE, true)
                supportFragmentManager.attachFragment(fl_main_container.id, EventsFragment.newInstance(), tag, isAddToBackStack)
                true
            }
            CalendarFragment.TAG -> {
                presenter.logEvent(AnalyticsService.SCR_OPEN_MY_EVENTS)
                presenter.addFragmentToStack(R.id.menu_calendar)
                supportFragmentManager.attachFragment(fl_main_container.id, CalendarFragment.newInstance(), tag, isAddToBackStack)
                true
            }
            PartnersFragment.TAG -> {
                presenter.logEvent(AnalyticsService.SCR_OPEN_PARTNERS)
                presenter.addFragmentToStack(R.id.menu_partner)
                supportFragmentManager.attachFragment(fl_main_container.id, PartnersFragment.newInstance(), tag, isAddToBackStack)
                true
            }
            ProfileHostFragment.TAG -> {
                if (presenter.isAuth) {
                    presenter.logEvent(AnalyticsService.SCR_OPEN_PROFILE)
                } else {
                    presenter.logEvent(AnalyticsService.SCR_OPEN_LOGIN)
                }
                presenter.addFragmentToStack(R.id.menu_profile)
                supportFragmentManager.attachFragment(fl_main_container.id, ProfileHostFragment.newInstance(), tag, isAddToBackStack)
                true
            }
            EventFragment.TAG -> {
                presenter.logEvent(AnalyticsService.SCR_OPEN_EVENT)
                supportFragmentManager.attachFragment(fl_main_container.id, EventFragment.newInstance(extras), tag, isAddToBackStack, sharedElements)
                true
            }
            PartnerFragment.TAG -> {
                if (extras?.getParcelable<Parcelable?>(PartnersFragment.PARTNER_MODEL) is PartnerOfferViewModel) {
                    presenter.logEvent(AnalyticsService.SCR_OPEN_PARTNER)
                } else if (extras?.getParcelable<Parcelable?>(PartnersFragment.PARTNER_MODEL) is TicketOfferViewModel) {
                    presenter.logEvent(AnalyticsService.SCR_OPEN_TICKET)
                }
                supportFragmentManager.attachFragment(fl_main_container.id, PartnerFragment.newInstance(extras), tag, isAddToBackStack, sharedElements)
                true
            }
            PromocodeFragment.TAG -> {
                presenter.logEvent(AnalyticsService.SCR_OPEN_PROMOCODE)
                colorizeStatusBar(ContextCompat.getColor(this, R.color.white_f2), true)
                supportFragmentManager.attachFragment(fl_main_container.id, PromocodeFragment.newInstance(), tag, isAddToBackStack)
                true
            }
            SubscriptionFragment.TAG -> {
                presenter.logEvent(AnalyticsService.SCR_OPEN_CLUB)
                colorizeStatusBar(Color.BLACK, false)
                supportFragmentManager.attachFragment(fl_main_container.id, SubscriptionFragment.newInstance(), tag, isAddToBackStack)
                true
            }
            SubscriptionFinalFragment.TAG -> {
                when (extras?.getBoolean(SubscriptionFinalFragment.IS_SUCCESS)) {
                    true -> presenter.logEvent(AnalyticsService.SCR_OPEN_PAYMENT_SUCCESS)
                    false -> presenter.logEvent(AnalyticsService.SCR_OPEN_PAYMENT_ERROR)
                    else -> {/* unused stub */}
                }

                colorizeStatusBar(ContextCompat.getColor(this, R.color.white_f2), true)
                supportFragmentManager.attachFragment(fl_main_container.id, SubscriptionFinalFragment.newInstance(extras), tag, isAddToBackStack)
                true
            }
            SignInFragment.TAG -> {
                presenter.logEvent(AnalyticsService.SCR_OPEN_LOGIN)
                colorizeStatusBar(ContextCompat.getColor(this, R.color.white_f2), true)
                supportFragmentManager.attachFragment(fl_main_container.id, SignInFragment.newInstance(extras), tag, isAddToBackStack)
                true
            }
            SplashFragment.TAG -> {
                colorizeStatusBar(Color.BLACK, false)
                supportFragmentManager.attachFragment(fl_main_container.id, SplashFragment.newInstance(), tag, isAddToBackStack)
                true
            }
            WebFragment.TAG -> {
                colorizeStatusBar(Color.WHITE, true)
                supportFragmentManager.attachFragment(fl_main_container.id, WebFragment.newInstance(extras), tag, isAddToBackStack)
                true
            }
            else -> false
        }
    }

    /**
     * Инициализация facebook-логина
     */
    fun loginFacebook() {
        LoginManager.getInstance().logOut()
        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                presenter.onFacebookSignIn(loginResult)
                (supportFragmentManager.primaryNavigationFragment as? ProfileHostFragment)?.let {
                    (it.childFragmentManager.primaryNavigationFragment as? SignInFragment)?.showProgress(true)
                } ?: (supportFragmentManager.primaryNavigationFragment as? SignInFragment)?.showProgress(true)
            }

            override fun onCancel() {
            }

            override fun onError(error: FacebookException) {
                Snackbar.make(cl_main_root, error.message ?: getString(R.string.sign_in_error), Snackbar.LENGTH_LONG).show()
            }
        })
        LoginManager.getInstance().logInWithReadPermissions(this@MainActivity, null)
    }

    /**
     * Обработка события выхода из профиля
     */
    fun logout() {
        (supportFragmentManager.primaryNavigationFragment as? ProfileHostFragment)?.updateFragment()
    }

    /**
     * Показывать ли [com.google.android.material.bottomnavigation.BottomNavigationView] на вновь открытом экране
     * @param isShow - флаг показа (true - показывать, false - скрыть)
     */
    fun showNavigationBar(isShow: Boolean) {
        bnv_main_navigation.visibility = if (isShow) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    /**
     * Обрабатывает повторное нажатие на активную вкладку.
     * Если экран реализует интерфейс [ScrollAware], то происходит вызов [ScrollAware.onScrollOnTop]
     */
    private fun tapOnOpenedTab() {
        (supportFragmentManager.primaryNavigationFragment as? ScrollAware)?.onScrollOnTop()
    }

    /**
     * Окрашивание статус-бара в переданный цвет (только с Android 6+)
     * Для Android 5 происходит только окрашивание статус-барав белый, иконки не видны
     * Для систем от Android 8+ происходит также окрашивание navigation bar-а с инвертацией цвета иконок
     * @param color - цвет для окрашивания
     * @param isInvertIcons - инвертировать ли цвета иконок
     */
    private fun colorizeStatusBar(@ColorInt color: Int, isInvertIcons: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            window.statusBarColor = color
            window.navigationBarColor = color
            window.decorView.systemUiVisibility = if (isInvertIcons) {
                SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            } else {
                0
            }
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.statusBarColor = color
            window.decorView.systemUiVisibility = if (isInvertIcons) {
                View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            } else {
                0
            }
        }
    }

    companion object {
        private const val STATUS_BAR_COLOR = "status_bar_color"
        private const val STATUS_BAR_IS_LIGHT = "status_bar_is_light"
        private const val NAVIGATION_IS_VISIBLE = "navigation_is_visible"
    }
}
