package com.ub.platforma.ui.splash.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ub.platforma.R
import com.ub.platforma.ui.events.fragments.EventsFragment
import com.ub.platforma.ui.main.activities.MainActivity
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class SplashFragment : Fragment(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    private var updateJob: Job? = null
    private var isPaused = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        updateJob = launch {
            delay(1500)

            while (isPaused) {
                delay(100)
            }

            (activity as MainActivity?)?.apply {
                openScreen(EventsFragment.TAG, isAddToBackStack = false)
                showNavigationBar(true)
            }
        }
    }

    override fun onResume() {
        super.onResume()

        isPaused = false
    }

    override fun onPause() {
        super.onPause()

        isPaused = true
    }

    override fun onDestroy() {
        super.onDestroy()

        updateJob?.cancel()
    }

    companion object {
        const val TAG = "SplashFragment"

        fun newInstance(): SplashFragment {
            return SplashFragment()
        }
    }
}