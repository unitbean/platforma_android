package com.ub.platforma.ui.profile.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import coil.api.load
import com.platforma.repository.models.*
import com.ub.platforma.R
import com.ub.platforma.ui.main.activities.MainActivity
import com.ub.platforma.ui.profile.presenters.ProfilePresenter
import com.ub.platforma.ui.profile.views.ProfileView
import com.ub.platforma.ui.signin.fragments.createRestorePurchasesAlert
import com.ub.platforma.ui.subscription.fragments.SubscriptionFragment
import com.ub.platforma.ui.web.fragments.toPicUrl
import com.ub.platforma.utils.CircleTransform
import com.ub.utils.gone
import com.ub.utils.visible
import kotlinx.android.synthetic.main.component_toolbar.*
import kotlinx.android.synthetic.main.fragment_profile.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter

class ProfileFragment : MvpAppCompatFragment(), ProfileView, View.OnClickListener {

    @InjectPresenter lateinit var presenter: ProfilePresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as? MainActivity)?.setSupportActionBar(toolbar)
        (activity as? MainActivity)?.supportActionBar?.setTitle(R.string.profile_title)

        b_profile_logout.setOnClickListener(this)
        b_profile_restore_purchases.setOnClickListener(this)

        presenter.loadProfile()
    }

    override fun onProfileLoaded(user: UserModel) {
        tv_profile_name.text = String.format(
            getString(R.string.profile_template),
            user.name ?: getString(R.string.profile_template_name_empty),
            user.lastName ?: getString(R.string.profile_template_last_name_empty))

        if (!user.avatar.isNullOrEmpty()) {
            iv_profile_avatar.load(user.avatar?.toPicUrl()) {
                transformations(CircleTransform())
            }
        }

        tv_profile_ads_message.text = when (user.premiumStatus) {
            PremiumStatus.NOT_PREMIUM -> getString(R.string.profile_adv_not_active)
            PremiumStatus.TRIAL -> String.format(getString(R.string.profile_ads_trial), presenter.trialRemainsDays)
            PremiumStatus.PREMIUM -> String.format(getString(R.string.profile_ads_active), presenter.endPremiumDate)
            else -> getString(R.string.profile_adv_not_active)
        }

        if (user.premiumStatus == PremiumStatus.NOT_PREMIUM) {
            tv_profile_ads_more.visible
            ll_profile_ads_block.setOnClickListener(this)
        } else {
            ll_profile_ads_block.setOnClickListener(null)
            tv_profile_ads_more.gone
        }
    }

    override fun onLogout() {
        (requireActivity() as MainActivity).logout()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.b_profile_logout -> presenter.logout()
            R.id.ll_profile_ads_block -> (requireActivity() as MainActivity).openScreen(SubscriptionFragment.TAG)
            R.id.b_profile_restore_purchases -> createRestorePurchasesAlert()?.show()
        }
    }

    companion object {
        const val TAG = "ProfileFragment"

        fun newInstance(): ProfileFragment {
            return ProfileFragment()
        }
    }
}