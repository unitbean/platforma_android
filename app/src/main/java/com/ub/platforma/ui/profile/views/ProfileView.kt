package com.ub.platforma.ui.profile.views

import com.platforma.repository.models.UserModel
import com.ub.platforma.ui.base.views.BaseView
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(OneExecutionStateStrategy::class)
interface ProfileView : BaseView {
    fun onProfileLoaded(user: UserModel)
    fun onLogout()
}