package com.ub.platforma.ui.partner.exceptions

class NameAndPhoneRequiredException: Exception()
class NameRequiredException: Exception()
class PhoneRequiredException: Exception()