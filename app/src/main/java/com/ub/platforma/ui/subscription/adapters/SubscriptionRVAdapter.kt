package com.ub.platforma.ui.subscription.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ub.platforma.R
import com.ub.platforma.ui.subscription.interfaces.SubscriptionClickListener
import com.ub.platforma.ui.subscription.models.*
import com.ub.utils.base.BaseListAdapter
import kotlinx.android.synthetic.main.rv_subscription_header.view.*
import kotlinx.android.synthetic.main.rv_subscription_item.view.*
import kotlinx.android.synthetic.main.rv_subscription_manage.view.*
import kotlinx.android.synthetic.main.rv_subscription_promocode.view.*

class SubscriptionRVAdapter : BaseListAdapter<ISubscription, RecyclerView.ViewHolder>() {

    var clickListener: SubscriptionClickListener? = null

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is HeaderViewModel -> R.layout.rv_subscription_header
            is SubscriptionViewModel -> R.layout.rv_subscription_item
            is PromocodeViewModel -> R.layout.rv_subscription_promocode
            is ManageViewModel -> R.layout.rv_subscription_manage
            else -> -1
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)

        return when (viewType) {
            R.layout.rv_subscription_header -> HeaderViewHolder(view)
            R.layout.rv_subscription_item -> SubscriptionViewHolder(view)
            R.layout.rv_subscription_promocode -> PromocodeViewHolder(view)
            R.layout.rv_subscription_manage -> ManageViewHolder(view)
            else -> throw IllegalStateException("Unknown ISubscription implementation")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is SubscriptionViewHolder) {
            holder.bind(position)
        }
    }

    inner class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private val ivClose: ImageView = view.iv_subscription_close

        init {
            ivClose.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            listener?.onClick(v, adapterPosition)
        }
    }

    inner class SubscriptionViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private val llItem: LinearLayout = view.b_subscription_item
        private val tvPrice: TextView = view.tv_subscription_price
        private val tvDiscount: TextView = view.tv_subscription_discount
        private val tvPeriod: TextView = view.tv_subscription_period

        init {
            llItem.setOnClickListener(this)
        }

        fun bind(position: Int) {
            val subscription = getItem(position) as SubscriptionViewModel

            tvPrice.text = String.format(itemView.context.getString(R.string.subscription_price_template), subscription.price)
            tvPeriod.text = subscription.title
            tvDiscount.text = String.format(itemView.context.getString(R.string.subscription_price_template), subscription.previousPrice)
        }

        override fun onClick(v: View) {
            clickListener?.onSubscriptionClick(getItem(adapterPosition))
        }
    }

    inner class PromocodeViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private val btnAction: Button = view.b_promocode_enter

        init {
            btnAction.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            clickListener?.onSubscriptionClick(getItem(adapterPosition))
        }
    }

    inner class ManageViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private val btnManage: Button = view.b_subscription_management

        init {
            btnManage.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            clickListener?.onSubscriptionClick(getItem(adapterPosition))
        }
    }
}