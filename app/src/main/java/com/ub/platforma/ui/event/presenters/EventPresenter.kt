package com.ub.platforma.ui.event.presenters

import com.ub.platforma.di.DIManager
import com.ub.platforma.ui.base.presenters.BasePresenter
import com.ub.platforma.ui.event.interactors.EventInteractor
import com.ub.platforma.ui.event.views.EventView
import com.ub.utils.LogUtils
import kotlinx.coroutines.launch
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class EventPresenter : BasePresenter<EventView>() {

    @Inject lateinit var interactor: EventInteractor

    var isParticipant: Boolean = false
    val isPremiumUser: Boolean
        get() = interactor.isPremiumUser()

    init {
        DIManager.getEventSubcomponent().inject(this)
    }

    override fun onDestroy() {
        DIManager.clearEventSubcomponent()
    }

    fun checkParticipant(eventId: String) {
        launch {
            try {
                isParticipant = interactor.isParticipant(eventId)
                viewState.setParticipantStatus(isParticipant)
            } catch (e: Exception) {
                LogUtils.e(TAG, e.message, e)
            }
        }
    }

    fun sendParticipant(eventId: String) {
        launch {
            try {
                val participant = interactor.changeParticipant(eventId, isParticipant)
                isParticipant = participant
                viewState.onParticipantChanged(participant)
            } catch (e: Exception) {
                LogUtils.e(TAG, e.message, e)
            }
        }
    }

    companion object {
        private const val TAG = "EventPresenter"
    }
}