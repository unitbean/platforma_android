package com.ub.platforma.ui.events.views

import com.ub.platforma.ui.base.views.BaseView
import com.ub.platforma.ui.events.models.IEventViewModel
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(OneExecutionStateStrategy::class)
interface EventsView : BaseView {
    fun onEventsLoaded(events: List<IEventViewModel>)
    fun onShowError(message: String?)
}