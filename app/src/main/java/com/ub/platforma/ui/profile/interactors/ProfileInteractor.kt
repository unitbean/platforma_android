package com.ub.platforma.ui.profile.interactors

import com.platforma.repository.models.UserModel
import com.platforma.repository.profile.IProfileRepository

class ProfileInteractor(private val repository: IProfileRepository) {

    val trialRemainingDays: Long
        get() = repository.getTrialRemainingDays()

    val endPremiumDate: String
        get() = repository.getEndPremiumDate()

    fun logout() {
        repository.logout()
    }

    suspend fun loadProfile(): UserModel {
        return if (repository.isUserLoaded()) {
            repository.getUser() ?: throw IllegalStateException("User not loaded")
        } else {
            val user = repository.updateUser(repository.getToken() ?: throw IllegalStateException("Token not loaded"))
            user ?: throw IllegalStateException("User not loaded")
        }
    }
}