package com.ub.platforma.ui.calendar.presenters

import com.ub.platforma.R
import com.ub.platforma.di.DIManager
import com.ub.platforma.ui.base.presenters.BasePresenter
import com.ub.platforma.ui.calendar.interactors.CalendarInteractor
import com.ub.platforma.ui.calendar.views.CalendarView
import com.ub.platforma.ui.events.models.EventViewModel
import com.ub.utils.LogUtils
import com.ub.utils.renew
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moxy.InjectViewState
import java.io.IOException
import javax.inject.Inject

@InjectViewState
class CalendarPresenter : BasePresenter<CalendarView>() {

    @Inject
    lateinit var interactor: CalendarInteractor

    val events = ArrayList<EventViewModel>()
    val isPremiumUser: Boolean
        get() = interactor.isPremiumUser()

    init {
        DIManager.getCalendarSubcomponent().inject(this)
    }

    fun updateUserEvents() {
        launch {
            try {
                val newEvents = withContext(Dispatchers.Default) { interactor.loadEvents() }
                events.renew(newEvents)
            } catch (e: Exception) {
                events.clear()
                LogUtils.e(TAG, e.message, e)
                if (e is IOException) {
                    viewState.onShowError(DIManager.appComponent.context.getString(R.string.error_network_connection))
                }
            }

            viewState.eventsUpdated(events)
        }
    }

    override fun onDestroy() {
        DIManager.clearCalendarSubcomponent()
    }

    companion object {
        private const val TAG = "CalendarPresenter"
    }
}