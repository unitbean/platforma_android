package com.ub.platforma.ui.event.fragments

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.CalendarContract
import android.provider.CalendarContract.Events
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.OvershootInterpolator
import android.view.animation.TranslateAnimation
import androidx.transition.TransitionInflater
import coil.api.load
import com.google.android.material.appbar.AppBarLayout
import com.ub.platforma.R
import com.ub.platforma.ui.event.presenters.EventPresenter
import com.ub.platforma.ui.event.views.EventView
import com.ub.platforma.ui.events.fragments.EventsFragment
import com.ub.platforma.ui.events.models.EventViewModel
import com.ub.platforma.ui.main.activities.MainActivity
import com.ub.platforma.ui.main.listeners.BackStackChangeListener
import com.ub.platforma.ui.subscription.fragments.SubscriptionFragment
import com.ub.platforma.ui.web.fragments.WebFragment
import com.ub.platforma.ui.web.fragments.toPicUrl
import com.ub.utils.dpToPx
import com.ub.utils.gone
import com.ub.utils.visible
import kotlinx.android.synthetic.main.fragment_event.*
import kotlinx.android.synthetic.main.fragment_event.view.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import java.text.SimpleDateFormat
import java.util.*

class EventFragment : MvpAppCompatFragment(), EventView, View.OnClickListener {

    private var event: EventViewModel? = null

    @InjectPresenter
    lateinit var presenter: EventPresenter

    private val dateFormatter by lazy { SimpleDateFormat("dd MMMM", Locale.getDefault()) }
    private val timeFormatter by lazy { SimpleDateFormat("HH:mm", Locale.getDefault()) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        sharedElementReturnTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_event, container, false)

        event = arguments?.getParcelable(EventsFragment.EVENT_MODEL)

        view.cl_event_root.transitionName = "card-${event?.id}"
        view.iv_event_image.transitionName = "image-${event?.id}"
        view.tv_event_type.transitionName = "type-${event?.id}"
        view.tv_event_title.transitionName = "title-${event?.id}"
        view.v_event_divider.transitionName = "divider-${event?.id}"
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        b_event_back.setOnClickListener(this)
        b_event_share.setOnClickListener(this)
        b_event_accept_participant.setOnClickListener(this)
        tv_event_date_time.setOnClickListener(this)
        tv_event_place.setOnClickListener(this)

        apl_event.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { _, offset ->
            b_event_back.alpha = 1 + offset.toFloat() / 2 / apl_event.height
            b_event_share.alpha = 1 + offset.toFloat() / 2 / apl_event.height
        })

        postponeEnterTransition()

        iv_event_image.load((event?.image ?: "").toPicUrl()) {
            error(R.drawable.ic_vector_pic_empty)
            listener { _, _ ->
                startPostponedEnterTransition()
                (requireActivity() as MainActivity).showNavigationBar(false)
                (activity as? BackStackChangeListener)?.onBackStackChange(this@EventFragment)
            }
        }

        tv_event_title.text = event?.title
        tv_event_type.text = event?.type

        tv_event_description.visible
        tv_event_place.visible

        @Suppress("DEPRECATION")
        tv_event_description.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(event?.description, Html.FROM_HTML_MODE_COMPACT)
        } else {
            Html.fromHtml(event?.description)
        }
        val price = event?.price ?: 0
        if (price > 0) {
            tv_event_price.visible
            tv_event_price.text = String.format(getString(R.string.event_price_format), price)

            if (event?.isFreeForPremium == true) {
                b_event_ads_offer.visible

                if (!presenter.isPremiumUser) {
                    b_event_ads_offer.setOnClickListener(this)
                } else {
                    b_event_ads_offer.setOnClickListener(null)
                    b_event_ads_offer.elevation = 0f
                    b_event_ads_offer.isClickable = false
                }
            } else {
                b_event_ads_offer.gone
            }
        } else {
            b_event_ads_offer.gone
            tv_event_price.gone
        }
        tv_event_place.text = event?.place

        event?.time?.let {
            tv_event_date_time.visible
            tv_event_date_time.text = String.format(
                getString(R.string.event_date_time_template),
                dateFormatter.format(Date(it)),
                timeFormatter.format(Date(it))
            )
        } ?: tv_event_date_time.gone

        if (!event?.linkToRegister.isNullOrEmpty()) {
            cl_event_content.setPadding(0, 0, 0, cl_event_content.dpToPx(76).toInt())
            b_event_accept_participant.visible

            val translateParticipate = TranslateAnimation(
                Animation.RELATIVE_TO_SELF,
                0.0f,
                Animation.RELATIVE_TO_SELF,
                0.0f,
                Animation.RELATIVE_TO_SELF,
                1.5f,
                Animation.RELATIVE_TO_SELF,
                0.0f
            ).apply {
                duration = 500
                interpolator = OvershootInterpolator()
            }
            b_event_accept_participant.startAnimation(translateParticipate)

            presenter.checkParticipant(event?.id ?: "")
        } else {
            cl_event_content.setPadding(0, 0, 0, 0)
            b_event_accept_participant.gone
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.b_event_ads_offer -> (requireActivity() as MainActivity).openScreen(
                SubscriptionFragment.TAG
            )
            R.id.b_event_accept_participant -> presenter.sendParticipant(event?.id ?: "")
            R.id.b_event_back -> requireActivity().onBackPressed()
            R.id.b_event_share -> shareEvent()
            R.id.tv_event_date_time -> addToCalendar()
            R.id.tv_event_place -> navigateToAddress()
        }
    }

    override fun setParticipantStatus(isParticipant: Boolean) {
        if (isParticipant) {
            b_event_accept_participant.text = getString(R.string.event_participant_decline)
        } else {
            b_event_accept_participant.text = getString(R.string.event_participant_accept)
        }

        b_event_accept_participant.isActivated = isParticipant
    }

    override fun onParticipantChanged(isParticipant: Boolean) {
        if (isParticipant) {
            if (!event?.linkToRegister.isNullOrEmpty()) {
                (requireActivity() as MainActivity).openScreen(WebFragment.TAG, Bundle().apply {
                    putString(WebFragment.URL_TO_OPEN, event!!.linkToRegister)
                })
            }

            b_event_accept_participant.text = getString(R.string.event_participant_decline)
        } else {
            b_event_accept_participant.text = getString(R.string.event_participant_accept)
        }

        b_event_accept_participant.isActivated = isParticipant
    }

    private fun addToCalendar() {
        val intent = Intent(Intent.ACTION_INSERT)
            .setData(Events.CONTENT_URI)
            .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, event?.time)
            .putExtra(Events.TITLE, event?.title)
            .putExtra(Events.DESCRIPTION, event?.description)
            .putExtra(Events.EVENT_LOCATION, event?.place)
            .putExtra(Events.AVAILABILITY, Events.AVAILABILITY_BUSY)
        startActivity(intent)
    }

    private fun navigateToAddress() {
        if (event?.lon == null || event?.lat == null) return
        val intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse("geo:0,0?q=${event?.lon},${event?.lat} (${event?.place})")
        )
        startActivity(intent)
    }

    /**
     * Формирует [Intent] для системы с возможностью поделиться контентом
     * Если в событии будут заголовок, место, цена, ссылка на регистрацию - они будут включены в текст приглашения
     * Если тип события - лонгрид, то будет прикреплена прямая ссылка на чтение
     */
    private fun shareEvent() {
        val sharedContent = buildString {
            val title = String.format(getString(R.string.event_share_title), event?.title)
            append(title)

            if (!event?.place.isNullOrEmpty()) {
                val place = String.format(getString(R.string.event_share_place), event?.place)
                append(place)
            }

            if (event?.price ?: 0 != 0) {
                val price = String.format(getString(R.string.event_share_price), event?.price)
                append(price)
            }

            if (!event?.linkToRegister.isNullOrEmpty()) {
                val register = String.format(getString(R.string.event_share_register), event?.linkToRegister)
                append(register)
            }
        }
        val intent = Intent(Intent.ACTION_SEND).apply {
            type = "text/plain"
            putExtra(Intent.EXTRA_TEXT, sharedContent)
        }
        val chooser = Intent.createChooser(intent, getString(R.string.event_share_chooser_title))
        startActivity(chooser)
    }

    companion object {
        const val TAG = "EventFragment"

        fun newInstance(bundle: Bundle?): EventFragment {
            return EventFragment().apply {
                arguments = bundle
            }
        }
    }
}