package com.ub.platforma.ui.event.interactors

import com.platforma.repository.event.IEventRepository

class EventInteractor(private val repository: IEventRepository) {

    fun isAuth(): Boolean = repository.isUserAuth()

    fun isPremiumUser(): Boolean = repository.isPremiumUser()

    suspend fun isParticipant(eventId: String): Boolean = repository.isParticipant(eventId)

    suspend fun changeParticipant(eventId: String, participant: Boolean): Boolean {
        return repository.changeParticipant(eventId, participant)
    }
}