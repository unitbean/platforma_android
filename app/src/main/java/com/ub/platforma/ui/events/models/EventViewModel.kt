package com.ub.platforma.ui.events.models

import android.os.Parcelable
import com.platforma.repository.models.response.EventResponseItem
import com.ub.utils.base.DiffComparable
import kotlinx.android.parcel.Parcelize

interface IEventViewModel : DiffComparable

@Parcelize
data class EventViewModel(val id: String?,
                          val preview: String,
                          val image: String,
                          val title: String,
                          val description: String,
                          val time: Long?,
                          val place: String?,
                          val type: String?,
                          val isFreeForPremium: Boolean,
                          val price: Int?,
                          val linkToRegister: String?,
                          val lon: Float?,
                          val lat: Float?,
                          val allowContent: Boolean,
                          @EventResponseItem.EventType
                          val eventType: String): IEventViewModel, Parcelable {

    constructor(modelResponse: EventResponseItem) : this(
        modelResponse.id,
        modelResponse.previewPic,
        modelResponse.mainPic,
        modelResponse.title,
        modelResponse.description,
        modelResponse.startDate,
        modelResponse.placeTitle,
        modelResponse.typeTitle,
        modelResponse.freeForPremium,
        modelResponse.price,
        modelResponse.linkToRegister,
        modelResponse.lon,
        modelResponse.lat,
        modelResponse.allowContent,
        modelResponse.type
    )

    override fun getItemId(): Int = id?.hashCode() ?: 0
}

data class HeaderViewModel(val header: String) : IEventViewModel {
    override fun getItemId(): Int = header.hashCode()
}

data class AdsViewModel(val message: String) : IEventViewModel {
    override fun getItemId(): Int = message.hashCode()
}