package com.ub.platforma.ui.partner.views

import androidx.annotation.StringRes
import com.ub.platforma.ui.base.views.BaseView
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(OneExecutionStateStrategy::class)
interface TicketGetView : BaseView {
    fun onTicketBooked()
    fun onShowError(@StringRes resId: Int, details: String? = null)
}