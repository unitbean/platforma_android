package com.ub.platforma.ui.subscription.presenters

import com.platforma.repository.models.PremiumStatus
import com.ub.platforma.di.DIManager
import com.ub.platforma.ui.base.presenters.BasePresenter
import com.ub.platforma.ui.subscription.interactors.SubscriptionInteractor
import com.ub.platforma.ui.subscription.models.SubscriptionViewModel
import com.ub.platforma.ui.subscription.views.SubscriptionView
import com.ub.utils.LogUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class SubscriptionPresenter : BasePresenter<SubscriptionView>() {

    @Inject
    lateinit var interactor: SubscriptionInteractor

    init {
        DIManager.getSubscriptionSubcomponent().inject(this)
    }

    override fun onDestroy() {
        DIManager.clearSubscriptionSubcomponent()
    }

    fun initialize() {
        launch {
            try {
                val newModels = withContext(Dispatchers.IO) { interactor.initialize() }
                viewState.onAvailableSubscriptionsLoaded(newModels)
            } catch (e: Exception) {
                LogUtils.e(TAG, e.message, e)
            }
        }
    }

    fun updateSubscriptionForStatus(@PremiumStatus status: Int) {
        launch {
            try {
                val newModels = withContext(Dispatchers.IO) { interactor.updateForStatus(status) }
                viewState.onAvailableSubscriptionsLoaded(newModels)
            } catch (e: Exception) {
                LogUtils.e(TAG, e.message, e)
            }
        }
    }

    fun checkPromocodeAvailability() {
        val isSignIn = interactor.checkPromocodeAvailability()
        viewState.onPromocodeAvailable(isSignIn)
    }

    fun checkIsSignInAndGetBuyLink(subscription: SubscriptionViewModel) {
        launch {
            try {
                val isSignIn = interactor.checkSignIn()
                val buyLink = withContext(Dispatchers.IO) { interactor.checkIsSignInAndGetBuyLink(subscription) }
                viewState.onSignInCheckReady(isSignIn, subscription, buyLink)
            } catch (e: Exception) {
                LogUtils.e(TAG, e.message, e)
            }
        }
    }

    companion object {
        private const val TAG = "SubscriptionPresenter"
    }
}