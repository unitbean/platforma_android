package com.ub.platforma.ui.partners.models

import android.os.Parcelable
import com.ub.utils.base.DiffComparable

abstract class BaseOfferModel(
    val id: String,
    val title: String,
    val description: String,
    val previewText: String?,
    val previewPic: String?,
    val mainPic: String?,
    val logo: String?,
    val allow: Boolean?
) : DiffComparable, Parcelable