package com.ub.platforma.ui.subscription.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.platforma.repository.AnalyticsService
import com.platforma.repository.models.PremiumStatus
import com.platforma.repository.models.response.BuyLinkResponse
import com.ub.platforma.R
import com.ub.platforma.ui.main.activities.MainActivity
import com.ub.platforma.ui.main.listeners.BackStackChangeListener
import com.ub.platforma.ui.profilehost.fragments.ProfileHostFragment
import com.ub.platforma.ui.promocode.fragments.PromocodeFragment
import com.ub.platforma.ui.signin.fragments.SignInFragment
import com.ub.platforma.ui.signin.interfaces.SignInRedirectType
import com.ub.platforma.ui.subscription.adapters.SubscriptionRVAdapter
import com.ub.platforma.ui.subscription.dialogs.SubscriptionLoginDialog
import com.ub.platforma.ui.subscription.interfaces.SubscriptionClickListener
import com.ub.platforma.ui.subscription.interfaces.SubscriptionInfoClickListener
import com.ub.platforma.ui.subscription.models.ISubscription
import com.ub.platforma.ui.subscription.models.ManageViewModel
import com.ub.platforma.ui.subscription.models.SubscriptionViewModel
import com.ub.platforma.ui.subscription.models.PromocodeViewModel
import com.ub.platforma.ui.subscription.presenters.SubscriptionPresenter
import com.ub.platforma.ui.subscription.views.SubscriptionView
import com.ub.platforma.ui.web.fragments.WebFragment
import com.ub.platforma.ui.web.interfaces.WebOperation
import com.ub.utils.base.BaseClickListener
import kotlinx.android.synthetic.main.fragment_subscription.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter

class SubscriptionFragment : MvpAppCompatFragment(),
    SubscriptionView,
    BaseClickListener,
    SubscriptionClickListener,
    SubscriptionInfoClickListener {

    @InjectPresenter lateinit var presenter: SubscriptionPresenter

    private var infoDialog: SubscriptionLoginDialog? = null

    private val adapter: SubscriptionRVAdapter by lazy { SubscriptionRVAdapter().apply {
        this.listener = this@SubscriptionFragment
        this.clickListener = this@SubscriptionFragment
    } }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_subscription, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rv_subscription.adapter = adapter
        rv_subscription.setHasFixedSize(true)

        (activity as? BackStackChangeListener)?.onBackStackChange(this)

        presenter.initialize()
    }

    override fun onClick(view: View, position: Int) {
        when (view.id) {
            R.id.iv_subscription_close -> requireActivity().onBackPressed()
        }
    }

    override fun onSubscriptionClick(subscription: ISubscription) {
        when (subscription) {
            is ManageViewModel -> openSubscriptionsDetails(subscription)
            is SubscriptionViewModel -> openWebForSubscription(subscription)
            is PromocodeViewModel -> presenter.checkPromocodeAvailability()
        }
    }

    override fun onSubscriptionUpdated(@PremiumStatus status: Int) {
        presenter.updateSubscriptionForStatus(status)
    }

    override fun onAvailableSubscriptionsLoaded(models: List<ISubscription>) {
        adapter.update(models)
    }

    override fun onSignInCheckReady(isSignIn: Boolean, subscription: SubscriptionViewModel, buyLink: BuyLinkResponse?) {
        if (isSignIn) {
            buyLink?.let {
                (activity as? MainActivity)?.openScreen(WebFragment.TAG, Bundle().apply {
                    putString(WebFragment.URL_TO_OPEN, it.orderId)
                    putString(WebFragment.URL_TO_SUCCESS, it.successUrl)
                    putString(WebFragment.URL_TO_FAIL, it.errorUrl)
                    putString(WebFragment.URL_OPERATION, WebOperation.BUY_SUBSCRIPTION)
                    putString(AnalyticsService.FB_CONTENT_ID, subscription.id)
                    putString(AnalyticsService.FB_CONTENT_TYPE, "subscription")
                    putString(AnalyticsService.FB_CONTENT_DATA, subscription.article)
                    putInt(AnalyticsService.FB_CONTENT_PRICE, subscription.price)
                })
            }
        } else {
            if (infoDialog?.isAdded != true) {
                infoDialog = SubscriptionLoginDialog.newInstance(false, subscription, buyLink)
                infoDialog?.show(childFragmentManager, SubscriptionLoginDialog.TAG)
            }
        }
    }

    override fun onAuthorize(subscription: SubscriptionViewModel, buyLink: BuyLinkResponse?) {
        (activity as? MainActivity)?.openScreen(SignInFragment.TAG, Bundle().apply {
            putString(SignInFragment.REDIRECT_AFTER, SignInRedirectType.SUBSCRIPTION)
            putString(SignInFragment.SELECTED_ID, subscription.id)
            putBoolean(SignInFragment.IS_EMBEDDED_MODE, true)
            putString(AnalyticsService.FB_CONTENT_ID, subscription.id)
            putString(AnalyticsService.FB_CONTENT_TYPE, "subscription")
            putString(AnalyticsService.FB_CONTENT_DATA, subscription.article)
            putInt(AnalyticsService.FB_CONTENT_PRICE, subscription.price)
        })
    }

    override fun showPromocode() {
        (activity as? MainActivity)?.openScreen(SignInFragment.TAG, Bundle().apply {
            putString(SignInFragment.REDIRECT_AFTER, SignInRedirectType.PROMOCODE)
            putBoolean(SignInFragment.IS_EMBEDDED_MODE, true)
        })
    }

    override fun onPromocodeAvailable(isPromocodeAvailable: Boolean) {
        if (isPromocodeAvailable) {
            (activity as? MainActivity)?.openScreen(PromocodeFragment.TAG)
        } else {
            if (infoDialog?.isAdded != true) {
                infoDialog = SubscriptionLoginDialog.newInstance(true, null, null)
                infoDialog?.show(childFragmentManager, SubscriptionLoginDialog.TAG)
            }
        }
    }

    private fun openWebForSubscription(subscription: SubscriptionViewModel) {
        presenter.checkIsSignInAndGetBuyLink(subscription)
    }

    private fun openSubscriptionsDetails(model: ManageViewModel) {
        // TODO открытие настроек подписок
    }

    companion object {
        const val TAG = "SubscriptionFragment"

        fun newInstance(): SubscriptionFragment {
            return SubscriptionFragment()
        }
    }
}