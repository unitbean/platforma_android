package com.ub.platforma.ui.partner.interactors

import com.platforma.repository.models.request.TicketBookRequest
import com.platforma.repository.ticketget.ITicketGetRepository
import com.ub.platforma.ui.partner.exceptions.NameAndPhoneRequiredException
import com.ub.platforma.ui.partner.exceptions.NameRequiredException
import com.ub.platforma.ui.partner.exceptions.PhoneRequiredException
import com.ub.platforma.ui.partners.models.TicketOfferViewModel

class TicketGetInteractor(private val repository: ITicketGetRepository) {

    suspend fun validateAndBookTicket(name: String, phone: String, ticket: TicketOfferViewModel): Boolean {
        return if (name.isEmpty() && phone.isEmpty()) {
            throw NameAndPhoneRequiredException()
        } else if (name.isEmpty()) {
            throw NameRequiredException()
        } else if (phone.isEmpty()) {
            throw PhoneRequiredException()
        } else {
            repository.bookTicket(TicketBookRequest(ticket.ticketId, phone, name)).equals("Ok", true)
        }
    }
}