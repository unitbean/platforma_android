package com.ub.platforma.ui.partners.presenters

import com.ub.platforma.R
import com.ub.platforma.di.DIManager
import com.ub.platforma.ui.base.presenters.BasePresenter
import com.ub.platforma.ui.partners.interactors.PartnersInteractor
import com.ub.platforma.ui.partners.models.BaseOfferModel
import com.ub.platforma.ui.partners.views.PartnerItemView
import com.ub.utils.LogUtils
import com.ub.utils.renew
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moxy.InjectViewState
import java.io.IOException
import javax.inject.Inject

@InjectViewState
class PartnerItemPresenter : BasePresenter<PartnerItemView>() {

    @Inject lateinit var interactor: PartnersInteractor

    val offers = ArrayList<BaseOfferModel>()

    init {
        DIManager.getPartnersSubcomponent().inject(this)
    }

    override fun onDestroy() {
        DIManager.clearPartnersSubcomponent()
    }

    fun loadOffers(isInTicketMode: Boolean) {
        launch {
            try {
                val newOffers = withContext(Dispatchers.Default) {
                    if (isInTicketMode) {
                        interactor.loadTickets()
                    } else {
                        interactor.loadPartners()
                    }
                }
                offers.renew(newOffers)
            } catch (e: Exception) {
                offers.clear()
                LogUtils.e(TAG, e.message, e)
                if (e is IOException) {
                    viewState.onShowError(DIManager.appComponent.context.getString(R.string.error_network_connection))
                }
            }

            viewState.onPartnersLoad(offers)
        }
    }

    companion object {
        private const val TAG = "PartnerItemPresenter"
    }
}