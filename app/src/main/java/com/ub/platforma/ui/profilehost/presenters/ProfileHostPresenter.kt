package com.ub.platforma.ui.profilehost.presenters

import com.ub.platforma.di.DIManager
import com.ub.platforma.ui.base.presenters.BasePresenter
import com.ub.platforma.ui.profilehost.interactors.ProfileHostInteractor
import com.ub.platforma.ui.profilehost.views.ProfileHostView
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class ProfileHostPresenter : BasePresenter<ProfileHostView>() {

    @Inject lateinit var interactor: ProfileHostInteractor

    init {
        DIManager.getProfileHostSubcomponent().inject(this)
    }

    override fun onDestroy() {
        DIManager.clearProfileHostSubcomponent()
    }

    fun isShowSignIn(): Boolean {
        return !interactor.isUserLogged()
    }
}