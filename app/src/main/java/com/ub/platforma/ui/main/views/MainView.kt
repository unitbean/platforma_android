package com.ub.platforma.ui.main.views

import com.platforma.repository.models.response.BuyLinkResponse
import com.ub.platforma.ui.base.views.BaseView
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(OneExecutionStateStrategy::class)
interface MainView : BaseView {
    fun onFirstOpen()
    fun onLoginSuccess(isAuth: Boolean)
    fun onShowError(message: String?)
    fun onBuyLinkReady(
        buyLinkResponse: BuyLinkResponse,
        id: String,
        type: String,
        data: String,
        price: Int
    )
}