package com.ub.platforma.ui.events.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.ub.platforma.R
import com.ub.platforma.ui.events.interfaces.TransitionClickListener
import com.ub.platforma.ui.events.models.AdsViewModel
import com.ub.platforma.ui.events.models.EventViewModel
import com.ub.platforma.ui.events.models.HeaderViewModel
import com.ub.platforma.ui.events.models.IEventViewModel
import com.ub.platforma.ui.web.fragments.toPicUrl
import com.ub.utils.base.BaseListAdapter
import com.ub.utils.gone
import com.ub.utils.visible
import kotlinx.android.synthetic.main.rv_event_item.view.*
import java.text.SimpleDateFormat
import java.util.*

class EventsRVAdapter(private val isCompactMode: Boolean, private val isPremiumUser: Boolean) : BaseListAdapter<IEventViewModel, RecyclerView.ViewHolder>() {

    private val dateFormatter: SimpleDateFormat by lazy { SimpleDateFormat("dd.MM / HH:mm", Locale.getDefault()) }
    var transitionClickListener: TransitionClickListener? = null

    override fun getItemViewType(position: Int): Int {
        return when {
            getItem(position) is HeaderViewModel -> R.layout.rv_event_header
            getItem(position) is AdsViewModel -> R.layout.rv_event_ads_item
            else -> R.layout.rv_event_item
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return when(viewType) {
            R.layout.rv_event_header -> HeaderViewHolder(view)
            R.layout.rv_event_ads_item -> AdsViewHolder(view)
            R.layout.rv_event_item -> EventViewHolder(view)
            else -> EventViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is EventViewHolder) {
            holder.bind(getItem(position))
        }
    }

    private inner class EventViewHolder(view: View): RecyclerView.ViewHolder(view), View.OnClickListener {

        private val card: CardView = view.cv_events_card
        private val container: ConstraintLayout = view.cl_events_container
        private val progress: ProgressBar = view.pb_events_progress
        private val image: ImageView = view.iv_events_image
        private val type: TextView = view.tv_events_type
        private val title: TextView = view.tv_events_title
        private val details: TextView = view.tv_events_details
        private val price: TextView = view.tv_events_price
        private val divider: View = view.v_events_divider

        init {
            container.setOnClickListener(this)
        }

        fun bind(model: IEventViewModel) {
            progress.visible
            val event = model as EventViewModel

            image.load(event.image.toPicUrl()) {
                error(R.drawable.ic_vector_pic_empty)
                listener { _, _ ->
                    progress.gone
                }
            }

            if (!event.type.isNullOrEmpty()) {
                type.visible
                type.text = event.type
            } else {
                type.gone
            }

            card.transitionName = "card-${event.id}"
            image.transitionName = "image-${event.id}"
            type.transitionName = "type-${event.id}"
            title.transitionName = "title-${event.id}"
            divider.transitionName = "divider-${event.id}"

            title.text = event.title

            val detailInfo = buildString {
                val isContainsData = event.time != null && event.time != 0L
                if (isContainsData) {
                    append(dateFormatter.format(Date(event.time ?: 0L)))
                }

                if (!event.place.isNullOrEmpty()) {
                    if (isContainsData) {
                        append(" / ")
                    }

                    append(event.place)
                }
            }

            if (detailInfo.isNotEmpty()) {
                details.visible
                details.text = detailInfo
            } else {
                details.gone
            }

            if (event.price != null && event.price > 0) {
                price.visible

                if (event.isFreeForPremium) {
                    if (!isPremiumUser) {
                        price.text = String.format(itemView.context.getString(R.string.event_price_format), event.price)
                        price.isActivated = false
                    } else {
                        price.text = itemView.context.getString(R.string.event_price_free)
                        price.isActivated = true
                    }
                } else {
                    price.text = String.format(itemView.context.getString(R.string.event_price_format), event.price)
                    price.isActivated = false
                }
            } else {
                price.gone
            }

            if ((event.price == null && event.place == null && event.time == null) || isCompactMode) {
                divider.gone
            } else {
                divider.visible
            }

            if (isCompactMode) {
                image.gone
            }
        }

        override fun onClick(v: View) {
            transitionClickListener?.onItemClick(v, adapterPosition, mapOf(
                card.transitionName to card,
                image.transitionName to image,
                type.transitionName to type,
                title.transitionName to title,
                divider.transitionName to divider
            ))
        }
    }

    private inner class AdsViewHolder(view: View): RecyclerView.ViewHolder(view), View.OnClickListener {

        private val button: Button = view.findViewById(R.id.b_events_ads_button)

        init {
            button.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            transitionClickListener?.onItemClick(v, adapterPosition, null)
        }
    }

    private inner class HeaderViewHolder(view: View): RecyclerView.ViewHolder(view)
}