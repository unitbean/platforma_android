package com.ub.platforma.ui.promocode.fragments

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.ub.platforma.R
import com.ub.platforma.ui.main.activities.MainActivity
import com.ub.platforma.ui.promocode.presenters.PromocodePresenter
import com.ub.platforma.ui.promocode.views.PromocodeView
import com.ub.platforma.ui.subscription.fragments.SubscriptionFinalFragment
import com.ub.platforma.ui.subscription.fragments.SubscriptionFragment
import com.ub.platforma.utils.BackPressedAware
import com.ub.utils.LogUtils
import com.ub.utils.invisible
import com.ub.utils.visible
import kotlinx.android.synthetic.main.fragment_promocode.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter

class PromocodeFragment : MvpAppCompatFragment(), PromocodeView, View.OnClickListener, BackPressedAware {

    @InjectPresenter
    lateinit var presenter: PromocodePresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_promocode, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        iv_promocode_close.setOnClickListener(this)
        b_promocode_check.setOnClickListener(this)
        b_promocode_check.isActivated = true
        b_promocode_check.isEnabled = false

        et_promocode_field.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                b_promocode_check.isActivated = s?.isNotEmpty() != true
                b_promocode_check.isEnabled = s?.isNotEmpty() == true
                presenter.promocode = s?.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_promocode_close -> {
                context?.hideSoftKeyboard()
                activity?.onBackPressed()
            }
            R.id.b_promocode_check -> presenter.checkCode()
        }
    }

    override fun onShowProgress(isShow: Boolean) {
        b_promocode_check.isActivated = isShow
    }

    override fun onPromocodeResult(isAccepted: Boolean) {
        tv_promocode_error.invisible
        et_promocode_field.isActivated = !isAccepted

        if (isAccepted) {
            (activity as? MainActivity)?.openScreen(SubscriptionFinalFragment.TAG, Bundle().apply {
                putBoolean(SubscriptionFinalFragment.IS_SUCCESS, true)
                putString(SubscriptionFinalFragment.SOURCE_SCREEN, TAG)
            })
        }
    }

    override fun onShowActivateError(errorText: String) {
        if (errorText.isNotEmpty()) {
            tv_promocode_error.text = errorText
            tv_promocode_error.visible
        }
        et_promocode_field.isActivated = errorText.isNotEmpty()
    }

    override fun onBackPressed(): Boolean {
        (activity as? MainActivity)?.supportFragmentManager?.popBackStack(SubscriptionFragment.TAG, 0)
        return true
    }

    companion object {
        const val TAG = "PromocodeFragment"

        fun newInstance(): PromocodeFragment {
            return PromocodeFragment()
        }
    }
}

/**
 * Hide keyboard
 */
fun Context.hideSoftKeyboard() {
    try {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow((this as Activity?)?.currentFocus?.windowToken, 0)
        currentFocus?.clearFocus()
    } catch (e: NullPointerException) {
        LogUtils.e("KeyboardHide", "NULL point exception in input method service")
    }
}