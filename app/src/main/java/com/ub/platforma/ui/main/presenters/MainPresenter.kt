package com.ub.platforma.ui.main.presenters

import com.facebook.login.LoginResult
import com.ub.platforma.di.DIManager
import com.ub.platforma.ui.base.presenters.BasePresenter
import com.ub.platforma.ui.main.interactor.MainInteractor
import com.ub.platforma.ui.main.views.MainView
import com.ub.utils.LogUtils
import com.vk.api.sdk.auth.VKAccessToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import moxy.InjectViewState
import java.util.*
import javax.inject.Inject

@InjectViewState
class MainPresenter : BasePresenter<MainView>() {

    @Inject lateinit var interactor: MainInteractor

    val isAuth: Boolean
        get() = interactor.isAuth()

    val fragmentStack = Stack<Int>()

    init {
        DIManager.getMainSubcomponent().inject(this)
    }

    override fun onFirstViewAttach() {
        launch {
            try {
                withContext(Dispatchers.Default) { interactor.loadSavedUser() }
            } catch (e: Exception) {
                LogUtils.e(TAG, e.message, e)
            }
        }
        viewState.onFirstOpen()
    }

    override fun onDestroy() {
        DIManager.clearMainSubcomponent()
    }

    fun onVkSignIn(res: VKAccessToken) {
        launch {
            try {
                val isSuccess = interactor.onVkSignIn(res)
                if (isSuccess) withContext(Dispatchers.Default) { interactor.loadSavedUser() }
                viewState.onLoginSuccess(isSuccess)
            } catch (e: Exception) {
                LogUtils.e(TAG, e.message, e)
                viewState.onShowError(e.message)
            }
        }
    }

    fun onFacebookSignIn(res: LoginResult) {
        launch {
            try {
                val isSuccess = interactor.onFbSignIn(res)
                if (isSuccess) withContext(Dispatchers.Default) { interactor.loadSavedUser() }
                viewState.onLoginSuccess(isSuccess)
            } catch (e: Exception) {
                LogUtils.e(TAG, e.message, e)
                viewState.onShowError(e.message)
            }
        }
    }

    /**
     * Добавляет идентификтор фрагмента в [Stack] для осуществления корректной навигации
     * и предотвращения множественного открытия
     */
    fun addFragmentToStack(fragmentId: Int) {
        if (fragmentStack.contains(fragmentId)) {
            fragmentStack.remove(fragmentId)
        }

        fragmentStack.add(fragmentId)
    }

    fun logEvent(key: String) {
        interactor.logEvent(key)
    }

    fun loadBuyLinkFromSubscriptionId(id: String?, type: String?, data: String?, price: Int?) {
        launch {
            try {
                val response = interactor.getBuyLink(id)
                viewState.onBuyLinkReady(response, id!!, type!!, data!!, price!!)
            } catch (e: Exception) {
                LogUtils.e(TAG, e.message, e)
            }
        }
    }

    companion object {
        private const val TAG = "MainPresenter"
    }
}