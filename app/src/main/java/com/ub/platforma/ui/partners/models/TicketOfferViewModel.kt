package com.ub.platforma.ui.partners.models

import com.platforma.repository.models.response.TicketOfferResponseItem
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TicketOfferViewModel(val ticketId: String,
                                val ticketTitle: String,
                                val ticketDescription: String,
                                val ticketPreviewText: String?,
                                val ticketPreviewPic: String?,
                                val ticketMainPic: String?,
                                val ticketLogo: String?,
                                val date: String,
                                val countOfFreeTickets: Int,
                                val price: Int,
                                val prevPrice: Int,
                                @TicketOfferResponseItem.SendRequestStatus
                                val didSendRequest: String,
                                val ticketAllow: Boolean): BaseOfferModel(
    ticketId,
    ticketTitle,
    ticketDescription,
    ticketPreviewText,
    ticketPreviewPic,
    ticketMainPic,
    ticketLogo,
    ticketAllow
) {
    constructor(response: TicketOfferResponseItem): this(
        response.id,
        response.title,
        response.description,
        response.previewText,
        response.previewPic,
        response.mainPic,
        response.logo,
        response.date,
        response.countOfFreeTickets,
        response.prevPrice,
        response.prevPrice,
        response.didSendRequest,
        response.allow
    )

    override fun getItemId(): Int = id.hashCode()
}