package com.ub.platforma.ui.partners.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.ub.platforma.R
import com.ub.platforma.ui.events.interfaces.TransitionClickListener
import com.ub.platforma.ui.partners.models.BaseOfferModel
import com.ub.platforma.ui.web.fragments.toPicUrl
import com.ub.platforma.ui.web.fragments.toPictureUrlScaled
import com.ub.platforma.utils.CircleTransform
import com.ub.utils.base.BaseListAdapter
import kotlinx.android.synthetic.main.rv_partner_item.view.*

class PartnersRVAdapter : BaseListAdapter<BaseOfferModel, PartnersRVAdapter.PartnerViewHolder>() {

    var transitionClickListener: TransitionClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PartnerViewHolder = PartnerViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.rv_partner_item, parent, false))

    override fun onBindViewHolder(holder: PartnerViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class PartnerViewHolder(view: View): RecyclerView.ViewHolder(view), View.OnClickListener {

        private val container = view.cl_partners_container
        private val image = view.iv_partners_image
        private val gradient = view.v_partners_gradient
        private val logo = view.iv_partners_sponsor_logo
        private val title = view.tv_partners_sponsor_title
        private val description = view.tv_partners_sponsor_description

        init {
            container.setOnClickListener(this)
        }

        fun bind(model: BaseOfferModel) {
            image.load((model.mainPic ?: "").toPicUrl())

            title.text = model.title
            description.text = model.previewText

            image.transitionName = "image-${model.id}"
            gradient.transitionName = "gradient-${model.id}"
            logo.transitionName = "logo-${model.id}"
            title.transitionName = "title-${model.id}"
            description.transitionName = "description-${model.id}"

            logo.load((model.logo ?: "").toPictureUrlScaled()) {
                transformations(CircleTransform(2f, Color.WHITE))
            }
        }

        override fun onClick(v: View) {
            transitionClickListener?.onItemClick(v, adapterPosition, mapOf(
                image.transitionName to image,
                gradient.transitionName to gradient,
                logo.transitionName to logo,
                title.transitionName to title,
                description.transitionName to description
            ))
        }
    }
}