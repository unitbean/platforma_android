package com.ub.platforma.ui.web.fragments

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Bundle
import android.view.*
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import com.platforma.repository.AnalyticsService
import com.ub.platforma.BuildConfig
import com.ub.platforma.R
import com.ub.platforma.di.DIManager
import com.ub.platforma.ui.events.fragments.EventsFragment
import com.ub.platforma.ui.events.models.EventViewModel
import com.ub.platforma.ui.main.activities.MainActivity
import com.ub.platforma.ui.promocode.fragments.hideSoftKeyboard
import com.ub.platforma.ui.subscription.fragments.SubscriptionFinalFragment
import com.ub.platforma.ui.subscription.fragments.SubscriptionFragment
import com.ub.platforma.ui.web.interfaces.WebOperation
import com.ub.platforma.utils.BackPressedAware
import com.ub.utils.argument
import com.ub.utils.gone
import com.ub.utils.visible
import kotlinx.android.synthetic.main.component_toolbar.*
import kotlinx.android.synthetic.main.fragment_web.*
import javax.inject.Inject

class WebFragment : Fragment(), BackPressedAware {

    @Inject lateinit var analyticsService: AnalyticsService

    private val orderIdOrParticipantUrl: String by argument(URL_TO_OPEN)
    private val successUrl: String by argument(URL_TO_SUCCESS)
    private val failUrl: String by argument(URL_TO_FAIL)
    private val operation: String by argument(URL_OPERATION, WebOperation.EVENT_REGISTER)
    private val longread: EventViewModel? by argument(EventsFragment.EVENT_MODEL)
    private var sourceScreen: String? = null

    init {
        DIManager.appComponent.injectToWeb(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_web, container, false)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val titleText = when (operation) {
            WebOperation.EVENT_REGISTER -> getString(R.string.web_title_event_register)
            WebOperation.BUY_SUBSCRIPTION -> getString(R.string.web_title_buy_subscription)
            WebOperation.READ_LONGREAD -> longread?.title
            else -> getString(R.string.web_title_manage_subscription)
        }

        if (arguments?.containsKey(AnalyticsService.FB_CONTENT_PRICE) == true &&
            arguments?.containsKey(AnalyticsService.FB_CONTENT_DATA) == true &&
            arguments?.containsKey(AnalyticsService.FB_CONTENT_ID) == true &&
            arguments?.containsKey(AnalyticsService.FB_CONTENT_TYPE) == true) {
            analyticsService.logViewContentEvent(
                arguments?.getString(AnalyticsService.FB_CONTENT_TYPE)!!,
                arguments?.getString(AnalyticsService.FB_CONTENT_DATA)!!,
                arguments?.getString(AnalyticsService.FB_CONTENT_ID)!!,
                arguments?.getInt(AnalyticsService.FB_CONTENT_PRICE)?.toDouble()!!
            )
        }

        sourceScreen = arguments?.getString(SubscriptionFinalFragment.SOURCE_SCREEN, null)

        (activity as? MainActivity)?.setSupportActionBar(toolbar)
        (activity as? MainActivity)?.supportActionBar?.title = titleText
        (activity as? MainActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as? MainActivity)?.supportActionBar?.setHomeButtonEnabled(true)

        pb_web.progress = 1
        val urlToLoad = when (operation) {
            WebOperation.BUY_SUBSCRIPTION -> orderIdOrParticipantUrl.toLinkToPay()
            WebOperation.READ_LONGREAD -> (longread?.id ?: "").toLinkToLongRead()
            else -> orderIdOrParticipantUrl
        }
        wv_web_content.loadUrl(urlToLoad)
        wv_web_content.settings.javaScriptEnabled = true
        wv_web_content.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                super.onProgressChanged(view, newProgress)
                pb_web?.progress = newProgress
            }
        }
        wv_web_content.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                return if (operation == WebOperation.BUY_SUBSCRIPTION) {
                    when {
                        request?.url.toString().contains(successUrl) -> {
                            (activity as? MainActivity)?.openScreen(SubscriptionFinalFragment.TAG, Bundle().apply {
                                putBoolean(SubscriptionFinalFragment.IS_SUCCESS, true)
                                putString(SubscriptionFinalFragment.SOURCE_SCREEN, sourceScreen ?: TAG)
                            })
                            if (arguments?.containsKey(AnalyticsService.FB_CONTENT_PRICE) == true &&
                                arguments?.containsKey(AnalyticsService.FB_CONTENT_DATA) == true &&
                                arguments?.containsKey(AnalyticsService.FB_CONTENT_ID) == true &&
                                arguments?.containsKey(AnalyticsService.FB_CONTENT_TYPE) == true) {
                                analyticsService.logPurchaseComplete(
                                    arguments?.getInt(AnalyticsService.FB_CONTENT_PRICE)?.toDouble()!!,
                                    arguments?.getString(AnalyticsService.FB_CONTENT_TYPE)!!,
                                    arguments?.getString(AnalyticsService.FB_CONTENT_DATA)!!,
                                    arguments?.getString(AnalyticsService.FB_CONTENT_ID)!!
                                )
                                analyticsService.logPurchase(
                                    arguments?.getInt(AnalyticsService.FB_CONTENT_PRICE)?.toLong()!! * 1000000,
                                    arguments?.getString(AnalyticsService.FB_CONTENT_ID)!!
                                )
                            }
                            false
                        }
                        request?.url.toString().contains(failUrl) -> {
                            (activity as? MainActivity)?.openScreen(SubscriptionFinalFragment.TAG, Bundle().apply {
                                putBoolean(SubscriptionFinalFragment.IS_SUCCESS, false)
                                putString(SubscriptionFinalFragment.SOURCE_SCREEN, TAG)
                            })
                            false
                        }
                        else -> super.shouldOverrideUrlLoading(view, request)
                    }
                } else false
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                pb_web?.progress = 1
                pb_web?.visible
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                pb_web?.progress = 100
                pb_web?.gone
            }
        }
    }

    override fun onBackPressed(): Boolean {
        (activity as? MainActivity)?.supportFragmentManager?.popBackStack(sourceScreen ?: SubscriptionFragment.TAG, 0)
        context?.hideSoftKeyboard()
        return false
    }

    companion object {
        const val TAG = "WebFragment"
        const val URL_TO_OPEN = "url_to_open"
        const val URL_TO_SUCCESS = "url_to_success"
        const val URL_TO_FAIL = "url_to_fail"
        const val URL_OPERATION = "url_opertaion"

        fun newInstance(bundle: Bundle?): WebFragment {
            return WebFragment().apply {
                this.arguments = bundle
            }
        }
    }
}

fun String.toPicUrl(): String {
    return "${BuildConfig.SERVER}/pics/$this"
}

fun String.toPictureUrlScaled(size: Int = 600): String {
    return "${BuildConfig.SERVER}/pics/$this?width=$size"
}

fun String.toLinkToPay(): String {
    return "${BuildConfig.SERVER}/api/v1/order/$this/pay"
}

fun String.toLinkToLongRead(): String {
    return "${BuildConfig.SERVER}/api/v1/event/$this/long-read"
}