package com.ub.platforma.ui.subscription.dialogs

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import com.platforma.repository.models.response.BuyLinkResponse
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.ub.platforma.R
import com.ub.platforma.ui.subscription.interfaces.SubscriptionInfoClickListener
import com.ub.platforma.ui.subscription.models.SubscriptionViewModel
import kotlinx.android.synthetic.main.dialog_subscription_info.view.*

class SubscriptionLoginDialog : BottomSheetDialogFragment(), View.OnClickListener {

    private var behavior: BottomSheetBehavior<*>? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)

        val view = View.inflate(context, R.layout.dialog_subscription_info, null)

        dialog.setContentView(view)
        dialog.setCanceledOnTouchOutside(true)
        dialog.window?.setWindowAnimations(R.style.DialogSlideAnimation)

        (view.parent as View).setBackgroundResource(android.R.color.transparent)

        val bottomSheet = dialog.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)
        behavior = BottomSheetBehavior.from(bottomSheet)
        behavior?.skipCollapsed = true
        behavior?.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    dismissAllowingStateLoss()
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })

        view.b_subscription_action.setOnClickListener(this)

        return dialog
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.b_subscription_action -> {
                val buyLink = arguments?.getParcelable<BuyLinkResponse?>(BUY_LINK)
                val subscription = arguments?.getParcelable<SubscriptionViewModel?>(SUBSCRIPTION)
                val isPromocode = arguments?.getBoolean(IS_PROMOCODE)

                if (isPromocode == false && subscription != null) {
                    (parentFragment as? SubscriptionInfoClickListener)?.onAuthorize(subscription, buyLink)
                } else if (isPromocode == true) {
                    (parentFragment as? SubscriptionInfoClickListener)?.showPromocode()
                }

                dismissAllowingStateLoss()
            }
        }
    }

    companion object {
        const val TAG = "SubscriptionLoginDialog"

        private const val IS_PROMOCODE = "is_promocode"
        private const val BUY_LINK = "buy_link"
        private const val SUBSCRIPTION = "subscription"

        fun newInstance(isPromoCode: Boolean, subscription: SubscriptionViewModel?, buyLink: BuyLinkResponse?): SubscriptionLoginDialog {
            return SubscriptionLoginDialog().apply {
                this.arguments = Bundle().apply {
                    putBoolean(IS_PROMOCODE, isPromoCode)
                    putParcelable(BUY_LINK, buyLink)
                    putParcelable(SUBSCRIPTION, subscription)
                }
            }
        }
    }
}