package com.ub.platforma.ui.calendar.interactors

import com.platforma.repository.calendar.ICalendarRepository
import com.ub.platforma.ui.events.models.EventViewModel

class CalendarInteractor(private val repository: ICalendarRepository) {

    suspend fun loadEvents(): List<EventViewModel> {
        return repository.loadEvents()
            .filter { event ->
                repository.getUserEvents().contains(event.id)
            }
            .map {
                EventViewModel(it)
            }
            .toMutableList()
    }

    fun isPremiumUser(): Boolean {
        return repository.isUserPremium()
    }
}