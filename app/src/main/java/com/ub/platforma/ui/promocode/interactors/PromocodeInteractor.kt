package com.ub.platforma.ui.promocode.interactors

import com.platforma.repository.models.UserModel
import com.platforma.repository.promocode.IPromocodeRepository
import com.ub.platforma.ui.promocode.exceptions.PromocodeIsNullOrBlank

class PromocodeInteractor(private val repository: IPromocodeRepository) {

    suspend fun checkPromocodeAndSave(promocode: String?): UserModel {
        if (promocode.isNullOrEmpty()) throw PromocodeIsNullOrBlank()
        return repository.sendPromocodeOnServer(promocode)
    }
}