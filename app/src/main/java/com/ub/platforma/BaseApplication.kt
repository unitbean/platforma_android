package com.ub.platforma

import android.app.Application
import com.ub.platforma.di.DIManager
import com.ub.platforma.di.components.DaggerAppComponent
import com.ub.utils.UbUtils
import com.crashlytics.android.Crashlytics
import com.ub.utils.LogUtils
import com.vk.api.sdk.VK
import io.fabric.sdk.android.Fabric

class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        Fabric.with(this, Crashlytics())

        DIManager.appComponent = DaggerAppComponent.builder()
            .context(applicationContext)
            .build()

        UbUtils.init(applicationContext)
        LogUtils.init(BuildConfig.DEBUG)
        LogUtils.setMessageLogger { Crashlytics.log(it) }
        LogUtils.setThrowableLogger { Crashlytics.logException(it) }
        VK.initialize(applicationContext)
    }
}