package com.ub.platforma.di.modules

import com.ub.platforma.di.scopes.PartnersScope
import com.platforma.repository.ApiService
import com.ub.platforma.ui.partners.interactors.PartnersInteractor
import com.platforma.repository.partners.PartnersRepository
import com.platforma.repository.partners.IPartnersRepository
import dagger.Module
import dagger.Provides

@Module
object PartnersModule {

    @JvmStatic
    @PartnersScope
    @Provides
    fun provideRepository(apiService: ApiService): IPartnersRepository {
        return PartnersRepository(apiService)
    }

    @JvmStatic
    @PartnersScope
    @Provides
    fun provideInteractor(repository: IPartnersRepository): PartnersInteractor {
        return PartnersInteractor(repository)
    }
}