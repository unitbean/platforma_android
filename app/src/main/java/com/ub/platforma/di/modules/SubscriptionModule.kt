package com.ub.platforma.di.modules

import com.ub.platforma.di.scopes.SubscriptionScope
import com.platforma.repository.ApiService
import com.platforma.repository.UserService
import com.ub.platforma.ui.subscription.interactors.SubscriptionInteractor
import com.platforma.repository.subscription.ISubscriptionRepository
import com.platforma.repository.subscription.SubscriptionRepository
import dagger.Module
import dagger.Provides

@Module
object SubscriptionModule {

    @JvmStatic
    @SubscriptionScope
    @Provides
    fun provideRepository(apiService: ApiService,
                          userService: UserService): ISubscriptionRepository {
        return SubscriptionRepository(apiService, userService)
    }

    @JvmStatic
    @SubscriptionScope
    @Provides
    fun provideInteractor(repository: ISubscriptionRepository): SubscriptionInteractor {
        return SubscriptionInteractor(repository)
    }
}