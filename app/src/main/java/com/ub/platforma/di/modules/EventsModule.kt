package com.ub.platforma.di.modules

import com.ub.platforma.di.scopes.EventsScope
import com.platforma.repository.ApiService
import com.platforma.repository.UserService
import com.ub.platforma.ui.events.interactors.EventsInteractor
import com.platforma.repository.events.EventsRepository
import com.platforma.repository.events.IEventsRepository
import dagger.Module
import dagger.Provides

@Module
object EventsModule {

    @JvmStatic
    @EventsScope
    @Provides
    fun providesRepository(apiService: ApiService, userService: UserService): IEventsRepository {
        return EventsRepository(apiService, userService)
    }

    @JvmStatic
    @EventsScope
    @Provides
    fun providesInteractor(repository: IEventsRepository): EventsInteractor {
        return EventsInteractor(repository)
    }
}