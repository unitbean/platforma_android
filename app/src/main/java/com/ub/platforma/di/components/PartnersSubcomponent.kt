package com.ub.platforma.di.components

import com.ub.platforma.di.modules.PartnersModule
import com.ub.platforma.di.scopes.PartnersScope
import com.ub.platforma.ui.partners.presenters.PartnerItemPresenter
import dagger.Subcomponent

@PartnersScope
@Subcomponent(modules = [PartnersModule::class])
interface PartnersSubcomponent {
    fun inject(presenter: PartnerItemPresenter)
}