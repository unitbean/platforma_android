package com.ub.platforma.di.modules

import com.platforma.repository.ApiService
import com.platforma.repository.ticketget.ITicketGetRepository
import com.platforma.repository.ticketget.TicketGetRepository
import com.ub.platforma.di.scopes.TicketGetScope
import com.ub.platforma.ui.partner.interactors.TicketGetInteractor
import dagger.Module
import dagger.Provides

@Module
object TicketGetModule {

    @JvmStatic
    @TicketGetScope
    @Provides
    fun provideRepository(apiService: ApiService): ITicketGetRepository {
        return TicketGetRepository(apiService)
    }

    @JvmStatic
    @TicketGetScope
    @Provides
    fun provideInteractor(repository: ITicketGetRepository): TicketGetInteractor {
        return TicketGetInteractor(repository)
    }
}