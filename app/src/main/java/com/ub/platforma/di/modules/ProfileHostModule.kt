package com.ub.platforma.di.modules

import com.ub.platforma.di.scopes.ProfileHostScope
import com.platforma.repository.UserService
import com.ub.platforma.ui.profilehost.interactors.ProfileHostInteractor
import com.platforma.repository.profilehost.IProfileHostRepository
import com.platforma.repository.profilehost.ProfileHostRepository
import dagger.Module
import dagger.Provides

@Module
object ProfileHostModule {

    @JvmStatic
    @ProfileHostScope
    @Provides
    fun provideRepository(userService: UserService): IProfileHostRepository {
        return ProfileHostRepository(userService)
    }

    @JvmStatic
    @ProfileHostScope
    @Provides
    fun provideInteractor(repository: IProfileHostRepository): ProfileHostInteractor {
        return ProfileHostInteractor(repository)
    }
}