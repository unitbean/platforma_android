package com.ub.platforma.di.components

import com.ub.platforma.di.modules.EventsModule
import com.ub.platforma.di.scopes.EventsScope
import com.ub.platforma.ui.events.presenters.EventsPresenter
import dagger.Subcomponent

@EventsScope
@Subcomponent(modules = [EventsModule::class])
interface EventsSubcomponent {
    fun inject(presenter: EventsPresenter)
}