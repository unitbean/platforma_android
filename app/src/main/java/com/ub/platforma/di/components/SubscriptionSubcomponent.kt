package com.ub.platforma.di.components

import com.ub.platforma.di.modules.SubscriptionModule
import com.ub.platforma.di.scopes.SubscriptionScope
import com.ub.platforma.ui.subscription.presenters.SubscriptionPresenter
import dagger.Subcomponent

@SubscriptionScope
@Subcomponent(modules = [SubscriptionModule::class])
interface SubscriptionSubcomponent {
    fun inject(presenter: SubscriptionPresenter)
}