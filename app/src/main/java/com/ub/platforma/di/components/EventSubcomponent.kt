package com.ub.platforma.di.components

import com.ub.platforma.di.modules.EventModule
import com.ub.platforma.di.scopes.EventScope
import com.ub.platforma.ui.event.presenters.EventPresenter
import dagger.Subcomponent

@EventScope
@Subcomponent(modules = [EventModule::class])
interface EventSubcomponent {
    fun inject(presenter: EventPresenter)
}