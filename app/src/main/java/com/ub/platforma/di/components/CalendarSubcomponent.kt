package com.ub.platforma.di.components

import com.ub.platforma.di.modules.CalendarModule
import com.ub.platforma.di.scopes.CalendarScope
import com.ub.platforma.ui.calendar.presenters.CalendarPresenter
import dagger.Subcomponent

@CalendarScope
@Subcomponent(modules = [CalendarModule::class])
interface CalendarSubcomponent {
    fun inject(presenter: CalendarPresenter)
}