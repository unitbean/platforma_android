package com.ub.platforma.di.components

import com.ub.platforma.di.modules.ProfileModule
import com.ub.platforma.di.scopes.ProfileScope
import com.ub.platforma.ui.profile.presenters.ProfilePresenter
import dagger.Subcomponent

@ProfileScope
@Subcomponent(modules = [ProfileModule::class])
interface ProfileSubcomponent {
    fun inject(presenter: ProfilePresenter)
}