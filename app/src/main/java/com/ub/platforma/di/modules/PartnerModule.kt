package com.ub.platforma.di.modules

import com.ub.platforma.di.scopes.PartnerScope
import com.platforma.repository.ApiService
import com.ub.platforma.ui.partner.interactors.PartnerInteractor
import com.platforma.repository.partner.IPartnerRepository
import com.platforma.repository.partner.PartnerRepository
import dagger.Module
import dagger.Provides

@Module
object PartnerModule {

    @JvmStatic
    @PartnerScope
    @Provides
    fun provideRepository(apiService: ApiService): IPartnerRepository {
        return PartnerRepository(apiService)
    }

    @JvmStatic
    @PartnerScope
    @Provides
    fun provideInteractor(repository: IPartnerRepository): PartnerInteractor {
        return PartnerInteractor(repository)
    }
}