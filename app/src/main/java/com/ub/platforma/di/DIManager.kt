package com.ub.platforma.di

import com.ub.platforma.di.modules.MainModule
import com.ub.platforma.di.components.*
import com.ub.platforma.di.modules.*

object DIManager {

    lateinit var appComponent: AppComponent
    private var mainSubcomponent: MainSubcomponent? = null
    private var eventsSubcomponent: EventsSubcomponent? = null
    private var partnersSubcomponent: PartnersSubcomponent? = null
    private var eventSubcomponent: EventSubcomponent? = null
    private var calendarSubcomponent: CalendarSubcomponent? = null
    private var subscriptionSubcomponent: SubscriptionSubcomponent? = null
    private var partnerSubcomponent: PartnerSubcomponent? = null
    private var profileSubcomponent: ProfileSubcomponent? = null
    private var profileHostSubcomponent: ProfileHostSubcomponent? = null
    private var promocodeSubcomponent: PromocodeSubcomponent? = null
    private var ticketGetSubcomponent: TicketGetSubcomponent? = null

    fun getMainSubcomponent(): MainSubcomponent {
        if (mainSubcomponent == null) {
            mainSubcomponent = appComponent.main(MainModule)
        }

        return mainSubcomponent ?: throw IllegalStateException("$mainSubcomponent must not be null")
    }

    fun clearMainSubcomponent() {
        mainSubcomponent = null
    }

    fun getEventsSubcomponent(): EventsSubcomponent {
        if (eventsSubcomponent == null) {
            eventsSubcomponent = appComponent.events(EventsModule)
        }

        return eventsSubcomponent ?: throw IllegalStateException("$eventsSubcomponent must not be null")
    }

    fun clearEventsSubcomponent() {
        eventsSubcomponent = null
    }

    fun getPartnersSubcomponent(): PartnersSubcomponent {
        if (partnersSubcomponent == null) {
            partnersSubcomponent = appComponent.partners(PartnersModule)
        }

        return partnersSubcomponent ?: throw IllegalStateException("$partnersSubcomponent must not be null")
    }

    fun clearPartnersSubcomponent() {
        partnersSubcomponent = null
    }

    fun getEventSubcomponent(): EventSubcomponent {
        if (eventSubcomponent == null) {
            eventSubcomponent = appComponent.event(EventModule)
        }

        return eventSubcomponent ?: throw IllegalStateException("$eventSubcomponent must not be null")
    }

    fun clearEventSubcomponent() {
        eventSubcomponent = null
    }

    fun getCalendarSubcomponent(): CalendarSubcomponent {
        if (calendarSubcomponent == null) {
            calendarSubcomponent = appComponent.calendar(CalendarModule)
        }

        return calendarSubcomponent ?: throw IllegalStateException("$calendarSubcomponent must not be null")
    }

    fun clearCalendarSubcomponent() {
        calendarSubcomponent = null
    }

    fun getSubscriptionSubcomponent(): SubscriptionSubcomponent {
        if (subscriptionSubcomponent == null) {
            subscriptionSubcomponent = appComponent.subscription(SubscriptionModule)
        }

        return subscriptionSubcomponent ?: throw IllegalStateException("$subscriptionSubcomponent must not be null")
    }

    fun clearSubscriptionSubcomponent() {
        subscriptionSubcomponent = null
    }

    fun getPartnerSubcomponent(): PartnerSubcomponent {
        if (partnerSubcomponent == null) {
            partnerSubcomponent = appComponent.partner(PartnerModule)
        }

        return partnerSubcomponent ?: throw IllegalStateException("$partnerSubcomponent must not be null")
    }

    fun clearPartnerSubcomponent() {
        partnerSubcomponent = null
    }

    fun getProfileSubcomponent(): ProfileSubcomponent {
        if (profileSubcomponent == null) {
            profileSubcomponent = appComponent.profile(ProfileModule)
        }

        return profileSubcomponent ?: throw IllegalStateException("$profileSubcomponent must not be null")
    }

    fun clearProfileSubcomponent() {
        profileSubcomponent = null
    }

    fun getProfileHostSubcomponent(): ProfileHostSubcomponent {
        if (profileHostSubcomponent == null) {
            profileHostSubcomponent = appComponent.profileHost(ProfileHostModule)
        }

        return profileHostSubcomponent ?: throw IllegalStateException("$profileHostSubcomponent must not be null")
    }

    fun clearProfileHostSubcomponent() {
        profileHostSubcomponent = null
    }

    fun getPromocodeSubcomponent(): PromocodeSubcomponent {
        if (promocodeSubcomponent == null) {
            promocodeSubcomponent = appComponent.promocode(PromocodeModule)
        }

        return promocodeSubcomponent ?: throw IllegalStateException("$promocodeSubcomponent must not be null")
    }

    fun clearPromocodeSubcomponent() {
        promocodeSubcomponent = null
    }

    fun getTicketGetSubcomponent(): TicketGetSubcomponent {
        if (ticketGetSubcomponent == null) {
            ticketGetSubcomponent = appComponent.ticketGet(TicketGetModule)
        }

        return ticketGetSubcomponent ?: throw IllegalStateException("$ticketGetSubcomponent must not be null")
    }

    fun clearTicketGetSubcomponent() {
        ticketGetSubcomponent = null
    }
}