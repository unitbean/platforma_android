package com.ub.platforma.di.components

import com.ub.platforma.di.modules.TicketGetModule
import com.ub.platforma.di.scopes.TicketGetScope
import com.ub.platforma.ui.partner.presenters.TicketGetPresenter
import dagger.Subcomponent

@TicketGetScope
@Subcomponent(modules = [TicketGetModule::class])
interface TicketGetSubcomponent {
    fun inject(presenter: TicketGetPresenter)
}