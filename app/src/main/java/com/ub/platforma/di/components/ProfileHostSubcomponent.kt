package com.ub.platforma.di.components

import com.ub.platforma.di.modules.ProfileHostModule
import com.ub.platforma.di.scopes.ProfileHostScope
import com.ub.platforma.ui.profilehost.presenters.ProfileHostPresenter
import dagger.Subcomponent

@ProfileHostScope
@Subcomponent(modules = [ProfileHostModule::class])
interface ProfileHostSubcomponent {
    fun inject(presenter: ProfileHostPresenter)
}