package com.ub.platforma.di.components

import com.ub.platforma.di.modules.PromocodeModule
import com.ub.platforma.di.scopes.PromocodeScope
import com.ub.platforma.ui.promocode.presenters.PromocodePresenter
import dagger.Subcomponent

@PromocodeScope
@Subcomponent(modules = [PromocodeModule::class])
interface PromocodeSubcomponent {
    fun inject(presenter: PromocodePresenter)
}