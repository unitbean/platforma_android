package com.ub.platforma.di.modules

import com.platforma.repository.AnalyticsService
import com.ub.platforma.ui.main.interactor.MainInteractor
import com.ub.platforma.di.scopes.MainScope
import com.platforma.repository.ApiService
import com.platforma.repository.UserService
import com.platforma.repository.main.IMainRepository
import com.platforma.repository.main.MainRepository
import dagger.Module
import dagger.Provides

@Module
object MainModule {

    @JvmStatic
    @MainScope
    @Provides
    fun provideRepository(apiService: ApiService,
                          userService: UserService,
                          analyticsService: AnalyticsService): IMainRepository {
        return MainRepository(apiService, userService, analyticsService)
    }

    @JvmStatic
    @MainScope
    @Provides
    fun provideInteractor(repository: IMainRepository): MainInteractor {
        return MainInteractor(repository)
    }
}