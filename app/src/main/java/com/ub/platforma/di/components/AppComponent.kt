package com.ub.platforma.di.components

import android.content.Context
import android.content.SharedPreferences
import com.ub.platforma.di.modules.MainModule
import com.ub.platforma.di.modules.CalendarModule
import com.ub.platforma.di.modules.*
import com.ub.platforma.ui.web.fragments.WebFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    val context: Context
    val preferences: SharedPreferences
    val prefsEditor: SharedPreferences.Editor

    fun injectToWeb(webFragment: WebFragment)

    fun main(module: MainModule): MainSubcomponent
    fun events(module: EventsModule): EventsSubcomponent
    fun partners(module: PartnersModule): PartnersSubcomponent
    fun event(module: EventModule): EventSubcomponent
    fun calendar(module: CalendarModule): CalendarSubcomponent
    fun subscription(module: SubscriptionModule): SubscriptionSubcomponent
    fun partner(module: PartnerModule): PartnerSubcomponent
    fun profile(module: ProfileModule): ProfileSubcomponent
    fun profileHost(module: ProfileHostModule): ProfileHostSubcomponent
    fun promocode(module: PromocodeModule): PromocodeSubcomponent
    fun ticketGet(module: TicketGetModule): TicketGetSubcomponent

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun context(context: Context): Builder

        fun build(): AppComponent
    }
}