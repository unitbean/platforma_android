package com.ub.platforma.di.modules

import com.ub.platforma.ui.profile.interactors.ProfileInteractor
import com.ub.platforma.di.scopes.ProfileScope
import com.platforma.repository.ApiService
import com.platforma.repository.UserService
import com.platforma.repository.profile.IProfileRepository
import com.platforma.repository.profile.ProfileRepository
import dagger.Module
import dagger.Provides

@Module
object ProfileModule {

    @JvmStatic
    @ProfileScope
    @Provides
    fun provideRepository(apiService: ApiService, userService: UserService): IProfileRepository {
        return ProfileRepository(apiService, userService)
    }

    @JvmStatic
    @ProfileScope
    @Provides
    fun provideInteractor(repository: IProfileRepository): ProfileInteractor {
        return ProfileInteractor(repository)
    }
}