package com.ub.platforma.di.modules

import android.content.Context
import android.content.SharedPreferences
import com.platforma.repository.AnalyticsService
import com.platforma.repository.ApiService
import com.platforma.repository.Preferences
import com.platforma.repository.UserService
import com.ub.platforma.BuildConfig
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Named
import javax.inject.Singleton

@Module
object AppModule {

    @JvmStatic
    @Provides
    @Singleton
    fun provideAnalytics(context: Context): AnalyticsService {
        return AnalyticsService(context)
    }

    @JvmStatic
    @Provides
    @Singleton
    fun provideCommonPreferences(context: Context): Preferences {
        return Preferences(context)
    }

    @JvmStatic
    @Provides
    @Singleton
    fun providePreferences(preferences: Preferences): SharedPreferences {
        return preferences.prefs
    }

    @JvmStatic
    @Provides
    @Singleton
    fun providePreferencesEditor(preferences: Preferences): SharedPreferences.Editor {
        return preferences.prefsEditor
    }

    @JvmStatic
    @Provides
    @Singleton
    fun provideUserService(
        prefs: SharedPreferences,
        editor: SharedPreferences.Editor
    ): UserService {
        return UserService(prefs, editor)
    }

    @JvmStatic
    @Provides
    @Singleton
    fun provideApi(@Named("deviceInterceptor") deviceInterceptor: Interceptor,
                   @Named("authInterceptor") authInterceptor: Interceptor,
                   @Named("loggingInterceptor") loggingInterceptor: Interceptor): ApiService {
        return ApiService(BuildConfig.SERVER, deviceInterceptor, authInterceptor, loggingInterceptor)
    }

    @JvmStatic
    @Provides
    @Singleton
    @Named("deviceInterceptor")
    fun providesDeviceInterceptor(userService: UserService) = object : Interceptor {
        override fun intercept(chain: Interceptor.Chain): Response {

            val original = chain.request()
            val method = original.method

            val requestBuilder = original.newBuilder()
                .method(method, original.body)

            requestBuilder.header("X-App-Version", BuildConfig.VERSION_NAME)
            requestBuilder.header("X-Device-Type", "ANDROID")

            if (!userService.firebaseToken.isNullOrEmpty()) {
                requestBuilder.header("X-Device-Token", userService.firebaseToken ?: "")
            }

            val request = requestBuilder.build()
            return chain.proceed(request)
        }
    }

    @JvmStatic
    @Provides
    @Singleton
    @Named("authInterceptor")
    fun providesAuthInterceptor(userService: UserService) = object : Interceptor {
        override fun intercept(chain: Interceptor.Chain): Response {
            val original = chain.request()

            return if (userService.isAuth()) {
                val method = original.method
                val requestBuilder = original.newBuilder()
                    .method(method, original.body)
                requestBuilder.header("X-Auth-Token", userService.userToken?.token ?: "UserIsNull")
                val request = requestBuilder.build()
                chain.proceed(request)
            } else {
                chain.proceed(original)
            }
        }
    }

    @JvmStatic
    @Provides
    @Singleton
    @Named("loggingInterceptor")
    fun providesLoggingInterceptor(): Interceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        return httpLoggingInterceptor.apply {
            httpLoggingInterceptor.level = if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor.Level.BODY
            } else {
                HttpLoggingInterceptor.Level.NONE
            }
        }
    }
}