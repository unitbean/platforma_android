package com.ub.platforma.di.modules

import com.platforma.repository.ApiService
import com.platforma.repository.UserService
import com.platforma.repository.promocode.IPromocodeRepository
import com.platforma.repository.promocode.PromocodeRepository
import com.ub.platforma.di.scopes.PromocodeScope
import com.ub.platforma.ui.promocode.interactors.PromocodeInteractor
import dagger.Module
import dagger.Provides

@Module
object PromocodeModule {

    @JvmStatic
    @PromocodeScope
    @Provides
    fun provideRepository(apiService: ApiService, userService: UserService): IPromocodeRepository {
        return PromocodeRepository(apiService, userService)
    }

    @JvmStatic
    @PromocodeScope
    @Provides
    fun provideInteractor(repository: IPromocodeRepository): PromocodeInteractor {
        return PromocodeInteractor(repository)
    }
}