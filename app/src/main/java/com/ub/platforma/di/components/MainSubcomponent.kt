package com.ub.platforma.di.components

import com.ub.platforma.di.modules.MainModule
import com.ub.platforma.di.scopes.MainScope
import com.ub.platforma.ui.main.presenters.MainPresenter
import dagger.Subcomponent

@MainScope
@Subcomponent(modules = [MainModule::class])
interface MainSubcomponent {
    fun inject(presenter: MainPresenter)
}