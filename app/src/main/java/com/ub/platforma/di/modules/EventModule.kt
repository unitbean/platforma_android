package com.ub.platforma.di.modules

import com.ub.platforma.di.scopes.EventScope
import com.platforma.repository.UserService
import com.ub.platforma.ui.event.interactors.EventInteractor
import com.platforma.repository.event.EventRepository
import com.platforma.repository.event.IEventRepository
import dagger.Module
import dagger.Provides

@Module
object EventModule {

    @JvmStatic
    @EventScope
    @Provides
    fun providesRepository(userService: UserService): IEventRepository {
        return EventRepository(userService)
    }

    @JvmStatic
    @EventScope
    @Provides
    fun providesInteractor(repository: IEventRepository): EventInteractor {
        return EventInteractor(repository)
    }
}