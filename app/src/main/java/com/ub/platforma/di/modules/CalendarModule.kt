package com.ub.platforma.di.modules

import com.platforma.repository.ApiService
import com.platforma.repository.UserService
import com.ub.platforma.ui.calendar.interactors.CalendarInteractor
import com.platforma.repository.calendar.CalendarRepository
import com.platforma.repository.calendar.ICalendarRepository
import dagger.Module
import dagger.Provides

@Module
object CalendarModule {

    @JvmStatic
    @com.ub.platforma.di.scopes.CalendarScope
    @Provides
    fun providesRepository(apiService: ApiService, userService: UserService): ICalendarRepository {
        return CalendarRepository(apiService, userService)
    }

    @JvmStatic
    @com.ub.platforma.di.scopes.CalendarScope
    @Provides
    fun providesInteractor(repository: ICalendarRepository): CalendarInteractor {
        return CalendarInteractor(repository)
    }
}