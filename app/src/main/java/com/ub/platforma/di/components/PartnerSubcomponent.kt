package com.ub.platforma.di.components

import com.ub.platforma.di.modules.PartnerModule
import com.ub.platforma.di.scopes.PartnerScope
import com.ub.platforma.ui.partner.presenters.PartnerPresenter
import dagger.Subcomponent

@PartnerScope
@Subcomponent(modules = [PartnerModule::class])
interface PartnerSubcomponent {
    fun inject(presenter: PartnerPresenter)
}