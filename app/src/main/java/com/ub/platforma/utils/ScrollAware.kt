package com.ub.platforma.utils

/**
 * Интерфейс, описывающий поведение фрагмента, поддерживающего скролл внутри себя
 */
interface ScrollAware {

    /**
     * Передача события перехода в верх списка
     */
    fun onScrollOnTop()
}