package com.ub.platforma.utils

import android.app.Notification
import android.media.RingtoneManager
import android.os.Build
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.ub.platforma.R
import com.ub.utils.UbNotify
import java.util.*

class FirebaseNotificationService : FirebaseMessagingService() {

    override fun onMessageReceived(event: RemoteMessage) {
        val title = event.notification?.title ?: getString(R.string.notification_default_title)
        val description = event.notification?.body ?: getString(R.string.notification_default_description)

        UbNotify
            .create(this, R.drawable.ic_platforma_notification, title, description)
            .setParams {
                setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            }
            .setChannelParams(
                getString(R.string.notification_default_channel_id),
                getString(R.string.notification_default_channel_description)
            ) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    enableVibration(true)
                    vibrationPattern = longArrayOf(500, 500)
                    lockscreenVisibility = Notification.VISIBILITY_PUBLIC
                }
            }
            .show(id = Random().nextInt())
    }
}