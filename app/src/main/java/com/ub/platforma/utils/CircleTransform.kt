package com.ub.platforma.utils

import android.graphics.*
import kotlin.math.min
import android.graphics.Bitmap
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import coil.bitmappool.BitmapPool
import coil.transform.Transformation
import com.ub.platforma.di.DIManager

// TODO поправить, чтобы единожды создавался Bitmap
class CircleTransform(private val strokeWidth: Float = 0F, private val strokeColor: Int = 0) : Transformation {

    override suspend fun transform(pool: BitmapPool, input: Bitmap): Bitmap {
        val circularBitmapDrawable = RoundedBitmapDrawableFactory.create(DIManager.appComponent.context.resources, input)
        circularBitmapDrawable.isCircular = true
        val bitmap = pool.get(input.width, input.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        circularBitmapDrawable.setBounds(0, 0, input.width, input.height)
        circularBitmapDrawable.draw(canvas)
        return bitmap.createBitmapWithBorder(strokeWidth, strokeColor)
    }

    override fun key(): String {
        return TRANSFORM_ID
    }

    companion object {
        private const val TRANSFORM_ID = "com.ub.platforma.utils.CircleTransform"

        fun Bitmap.createBitmapWithBorder(borderSize: Float, borderColor: Int = Color.WHITE): Bitmap {
            val borderOffset = (borderSize * 2).toInt()
            val halfWidth = width / 2
            val halfHeight = height / 2
            val circleRadius = min(halfWidth, halfHeight).toFloat()
            val newBitmap = Bitmap.createBitmap(
                width + borderOffset,
                height + borderOffset,
                Bitmap.Config.ARGB_8888
            )

            // Center coordinates of the image
            val centerX = halfWidth + borderSize
            val centerY = halfHeight + borderSize

            val paint = Paint()
            val canvas = Canvas(newBitmap).apply {
                // Set transparent initial area
                drawARGB(0, 0, 0, 0)
            }

            // Draw the transparent initial area
            paint.isAntiAlias = true
            paint.style = Paint.Style.FILL
            canvas.drawCircle(centerX, centerY, circleRadius, paint)

            // Draw the image
            paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
            canvas.drawBitmap(this, borderSize, borderSize, paint)

            // Draw the createBitmapWithBorder
            paint.xfermode = null
            paint.style = Paint.Style.STROKE
            paint.color = borderColor
            paint.strokeWidth = borderSize
            canvas.drawCircle(centerX, centerY, circleRadius, paint)
            return newBitmap
        }
    }
}
