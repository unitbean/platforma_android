package com.platforma.repository.events

import com.platforma.repository.ApiService
import com.platforma.repository.UserService
import com.platforma.repository.models.BaseResponse
import com.platforma.repository.models.response.EventResponseItem

class EventsRepository(private val api: ApiService, private val userService: UserService) :
    IEventsRepository {

    override suspend fun loadEvents(): BaseResponse<List<EventResponseItem>> = api.platforma.getEvents()

    override fun isPremiumUser(): Boolean = userService.isPremiumUser()
}