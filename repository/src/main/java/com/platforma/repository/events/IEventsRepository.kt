package com.platforma.repository.events

import com.platforma.repository.models.BaseResponse
import com.platforma.repository.models.response.EventResponseItem

interface IEventsRepository {
    suspend fun loadEvents(): BaseResponse<List<EventResponseItem>>
    fun isPremiumUser(): Boolean
}