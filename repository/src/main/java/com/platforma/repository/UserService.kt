package com.platforma.repository

import android.content.SharedPreferences
import com.facebook.login.LoginResult
import com.platforma.repository.models.*
import com.platforma.repository.models.request.AuthRequest
import com.platforma.repository.models.request.PromocodeRequest
import com.platforma.repository.models.response.UserResponse
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.ub.utils.renew
import com.vk.api.sdk.auth.VKAccessToken
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * Не внедрять сюда зависимость от [ApiService], иначе будет Dependency Cycle
 * Лучше использовать в методах явную передачу объекта [ApiService]
 *
 * Не хочется выносить в static-field конфликтые значения
 */
class UserService(private val prefs: SharedPreferences,
                  private val editor: SharedPreferences.Editor) {

    private val dateFormatter: SimpleDateFormat by lazy { SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.getDefault()) }
    private val shortDateFormatter: SimpleDateFormat by lazy { SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()) }
    private val subscriptionEvents = ArrayList<String>()
    var userToken: UserTokenModel? = null
        private set
    var user: UserModel? = null
        private set
    var firebaseToken: String? = null
        private set

    init {
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {
            firebaseToken = it.result?.token
        }
    }

    fun isParticipant(eventId: String): Boolean = subscriptionEvents.contains(eventId)

    suspend fun checkPromocodeAndSaveUser(apiService: ApiService, promocode: String): UserModel {
        return apiService.platforma.sendPromocodeAndCheck(PromocodeRequest(promocode)).result.toUserViewModel()
    }

    suspend fun forceUpdateUser(platformaApi: ApiService.Main): UserModel {
        val updatedUser = platformaApi.updateUser().result
        return updatedUser.toUserViewModel().apply {
            this@UserService.user = this
        }
    }

    fun changeParticipant(eventId: String, participant: Boolean): Boolean {
        return if (!participant) {
            subscriptionEvents.add(eventId)
            true
        } else {
            subscriptionEvents.remove(eventId)
            false
        }
    }

    fun loadEvents() {
        subscriptionEvents.renew(prefs.getStringSet(SAVED_EVENTS, setOf()) ?: setOf())
    }

    fun saveEvents() {
        editor.putStringSet(SAVED_EVENTS, subscriptionEvents.toSet()).apply()
    }

    fun restoreUserToken(): UserTokenModel? {
        val token = Gson().fromJson(prefs.getString(SAVED_USER, ""), UserTokenModel::class.java)
        this.userToken = token
        return token
    }

    suspend fun loginWithVk(platformaApi: ApiService.Main, res: VKAccessToken) {
        val updatedUser = platformaApi.getUser(AuthRequest(res.userId.toString(), res.accessToken, UserTokenModel.TokenType.VK)).result
        userToken = UserTokenModel(updatedUser.accessToken.token, res.userId.toString(), UserTokenModel.TokenType.VK)
        editor.putString(SAVED_USER, Gson().toJson(userToken)).apply()
    }

    suspend fun loginWithFb(platformaApi: ApiService.Main, res: LoginResult) {
        val updatedUser = platformaApi.getUser(AuthRequest(res.accessToken.userId, res.accessToken.token, UserTokenModel.TokenType.FB)).result
        userToken = UserTokenModel(updatedUser.accessToken.token, res.accessToken.userId, UserTokenModel.TokenType.FB)
        editor.putString(SAVED_USER, Gson().toJson(userToken)).apply()
    }

    fun saveUser(userModel: UserModel) {
        user = userModel
    }

    fun getUserEvents(): List<String> = subscriptionEvents

    fun isAuth(): Boolean = userToken != null

    fun isPremiumUser(): Boolean {
        return user?.premiumStatus == PremiumStatus.PREMIUM
    }

    fun getTrialRemainingDays(): Long {
        return 10L
    }

    fun getEndPremiumDate(): String {
        return user?.endPremiumDate ?: ""
    }

    fun cleanCachedProfile() {
        user = null
    }

    fun logout() {
        user = null
        userToken = null
        editor.clear().apply()
    }

    fun cleanData() {
        subscriptionEvents.clear()
        logout()
    }

    private fun UserResponse.toUserViewModel(): UserModel {
        return UserModel(id,
            firstName,
            lastName,
            pic,
            if (activeUntil == null) 0 else 2,
            try {
                val endDate = dateFormatter.parse(activeUntil)
                shortDateFormatter.format(endDate)
            } catch (e: Exception) {
                null
            })
    }

    companion object {
        private const val SAVED_USER = "saved_user_v2"
        private const val SAVED_EVENTS = "saved_events"
    }
}