package com.platforma.repository.ticketget

import com.platforma.repository.models.request.TicketBookRequest

interface ITicketGetRepository {
    suspend fun bookTicket(request: TicketBookRequest): String
}