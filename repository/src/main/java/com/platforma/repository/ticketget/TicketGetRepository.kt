package com.platforma.repository.ticketget

import com.platforma.repository.ApiService
import com.platforma.repository.models.request.TicketBookRequest

class TicketGetRepository(private val api: ApiService) : ITicketGetRepository {

    override suspend fun bookTicket(request: TicketBookRequest): String = api.platforma.bookTicket(request).result
}