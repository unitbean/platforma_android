package com.platforma.repository.profilehost

interface IProfileHostRepository {
    fun isUserLogged(): Boolean
}