package com.platforma.repository.profilehost

import com.platforma.repository.UserService

class ProfileHostRepository(private val userService: UserService) :
    IProfileHostRepository {

    override fun isUserLogged(): Boolean = userService.isAuth()
}