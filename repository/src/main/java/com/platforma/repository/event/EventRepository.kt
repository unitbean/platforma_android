package com.platforma.repository.event

import com.platforma.repository.UserService

class EventRepository(private val userManager: UserService) : IEventRepository {

    override fun isUserAuth(): Boolean = userManager.isAuth()

    override fun isPremiumUser(): Boolean = userManager.isPremiumUser()

    override suspend fun isParticipant(eventId: String): Boolean = userManager.isParticipant(eventId)

    override suspend fun changeParticipant(eventId: String, participant: Boolean): Boolean {
        val result =  userManager.changeParticipant(eventId, participant)
        userManager.saveEvents()
        return result
    }
}