package com.platforma.repository.event

interface IEventRepository {
    fun isUserAuth(): Boolean
    fun isPremiumUser(): Boolean
    suspend fun isParticipant(eventId: String): Boolean
    suspend fun changeParticipant(eventId: String, participant: Boolean): Boolean
}