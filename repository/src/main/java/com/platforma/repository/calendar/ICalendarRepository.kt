package com.platforma.repository.calendar

import com.platforma.repository.models.response.EventResponseItem

interface ICalendarRepository {
    suspend fun loadEvents(): List<EventResponseItem>
    suspend fun getUserEvents(): List<String>
    fun isUserLogged(): Boolean
    fun isUserPremium(): Boolean
}