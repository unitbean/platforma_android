package com.platforma.repository.calendar

import com.platforma.repository.ApiService
import com.platforma.repository.UserService
import com.platforma.repository.models.response.EventResponseItem

class CalendarRepository(private val api: ApiService, private val userManager: UserService):
    ICalendarRepository {

    override suspend fun loadEvents(): List<EventResponseItem> = api.platforma.getEvents().result

    override suspend fun getUserEvents(): List<String> = userManager.getUserEvents()

    override fun isUserLogged(): Boolean = userManager.isAuth()

    override fun isUserPremium(): Boolean = userManager.isPremiumUser()
}