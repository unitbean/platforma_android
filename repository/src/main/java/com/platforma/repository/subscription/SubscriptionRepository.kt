package com.platforma.repository.subscription

import com.platforma.repository.ApiService
import com.platforma.repository.UserService
import com.platforma.repository.models.response.BuyLinkResponse
import com.platforma.repository.models.response.SubscriptionResponse

class SubscriptionRepository(
    private val apiService: ApiService,
    private val userService: UserService): ISubscriptionRepository {

    override suspend fun receiveSubscriptions(): List<SubscriptionResponse>? = apiService.platforma.getSubscriptions().result

    override fun isUserTrialAvailable(): Boolean = true

    override fun isUserSignIn(): Boolean = userService.isAuth()

    override fun removeCachedProfile() = userService.cleanCachedProfile()

    override suspend fun receiveBuyLink(subscriptionId: String): BuyLinkResponse = apiService.platforma.startBuy(subscriptionId).result
}