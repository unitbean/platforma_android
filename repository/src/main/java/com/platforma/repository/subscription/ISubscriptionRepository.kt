package com.platforma.repository.subscription

import com.platforma.repository.models.response.BuyLinkResponse
import com.platforma.repository.models.response.SubscriptionResponse

interface ISubscriptionRepository {
    suspend fun receiveSubscriptions(): List<SubscriptionResponse>?
    suspend fun receiveBuyLink(subscriptionId: String): BuyLinkResponse
    fun isUserTrialAvailable(): Boolean
    fun isUserSignIn(): Boolean
    fun removeCachedProfile()
}