package com.platforma.repository.main

import com.facebook.login.LoginResult
import com.platforma.repository.models.UserModel
import com.platforma.repository.models.UserTokenModel
import com.platforma.repository.models.response.BuyLinkResponse
import com.vk.api.sdk.auth.VKAccessToken

interface IMainRepository {
    fun logEvent(key: String)
    suspend fun loginWithVk(res: VKAccessToken)
    suspend fun loginWithFb(res: LoginResult)
    fun saveUser(user: UserModel)
    fun isAuthSuccess(): Boolean
    fun loadEvents()
    fun logout()
    fun logLoginEventWithType(@UserTokenModel.TokenType type: String)
    suspend fun loadVkUser(): UserModel
    suspend fun loadFbUser(): UserModel
    suspend fun restoreUserToken(): UserTokenModel?
    suspend fun receiveBuyLink(subscriptionId: String): BuyLinkResponse
}