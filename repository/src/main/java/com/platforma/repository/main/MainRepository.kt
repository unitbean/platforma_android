package com.platforma.repository.main

import com.facebook.login.LoginResult
import com.platforma.repository.AnalyticsService
import com.platforma.repository.ApiService
import com.platforma.repository.UserService
import com.platforma.repository.models.UserModel
import com.platforma.repository.models.UserTokenModel
import com.platforma.repository.models.response.BuyLinkResponse
import com.vk.api.sdk.auth.VKAccessToken

class MainRepository(private val apiService: ApiService,
                     private val userService: UserService,
                     private val analytics: AnalyticsService) : IMainRepository {

    override fun logEvent(key: String) {
        analytics.logEvent(key)
    }

    override fun logLoginEventWithType(@UserTokenModel.TokenType type: String) {
        analytics.logCompleteRegistrationEvent(type)
    }

    override suspend fun loginWithVk(res: VKAccessToken) {
        userService.loginWithVk(apiService.platforma, res)
    }

    override suspend fun loginWithFb(res: LoginResult) {
        userService.loginWithFb(apiService.platforma, res)
    }

    override fun saveUser(user: UserModel) {
        userService.saveUser(user)
    }

    override fun logout() = userService.logout()

    override fun isAuthSuccess(): Boolean = userService.isAuth()

    override fun loadEvents() = userService.loadEvents()

    override suspend fun loadVkUser(): UserModel {
        val user = userService.forceUpdateUser(apiService.platforma)
        analytics.logEvent(AnalyticsService.EVENT_LOGIN, mapOf(AnalyticsService.EVENT_LOGIN_TYPE to AnalyticsService.EVENT_LOGIN_TYPE_VK))
        return user
    }

    override suspend fun loadFbUser(): UserModel {
        val user = userService.forceUpdateUser(apiService.platforma)
        analytics.logEvent(AnalyticsService.EVENT_LOGIN, mapOf(AnalyticsService.EVENT_LOGIN_TYPE to AnalyticsService.EVENT_LOGIN_TYPE_FB))
        return user
    }

    override suspend fun restoreUserToken(): UserTokenModel? = userService.restoreUserToken()

    override suspend fun receiveBuyLink(subscriptionId: String): BuyLinkResponse = apiService.platforma.startBuy(subscriptionId).result
}