package com.platforma.repository.models.response

class UserResponse(
    val id: String,
    // формат даты yyyy-MM-dd’T’HH:mm:ss.SSSXXX
    val activeUntil: String?,
    val pic: String,
    val firstName: String,
    val lastName: String
)