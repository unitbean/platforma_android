package com.platforma.repository.models.response

class AuthResponse(
    val accessToken: TokenResponse,
    val me: UserResponse
)