package com.platforma.repository.models.request

import com.platforma.repository.models.UserTokenModel

class AuthRequest(
    val socialNetworkId: String,
    val socialNetworkAccessToken: String,
    @UserTokenModel.TokenType
    val type: String
)