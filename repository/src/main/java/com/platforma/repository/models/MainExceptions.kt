package com.platforma.repository.models

import com.platforma.repository.R


@Suppress("UNUSED")
open class LoginException(val reason: String?, val localizedMessage: Int?): Throwable(message = reason)
data class InvalidTokenException(val errorMessage: String?): LoginException(errorMessage, R.string.profile_token_invalid)
data class TokenExpiredException(val errorMessage: String?): LoginException(errorMessage, R.string.profile_token_expired)
data class UndefinedException(val errorMessage: String?): LoginException(errorMessage, null)