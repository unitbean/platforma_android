package com.platforma.repository.models

class BaseResponse<T>(val result: T)