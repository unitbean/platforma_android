package com.platforma.repository.models.response

import android.os.Parcelable
import androidx.annotation.StringDef
import kotlinx.android.parcel.Parcelize

@Parcelize
class SubscriptionResponse(
    val id: String,
    val title: String,
    val article: String,
    val price: Int,
    val previousPrice: Int,
    val timeValue: Int,
    @SubscriptionPeriod
    val timeType: String
): Parcelable {
    @StringDef(
        SubscriptionPeriod.PERIOD_DAY,
        SubscriptionPeriod.PERIOD_MONTH,
        SubscriptionPeriod.PERIOD_YEAR)
    @Retention(AnnotationRetention.SOURCE)
    annotation class SubscriptionPeriod {
        companion object {
            const val PERIOD_DAY = "DAY"
            const val PERIOD_MONTH = "MONTH"
            const val PERIOD_YEAR = "YEAR"
        }
    }
}