package com.platforma.repository.models.response

class EventResponseItem(
    val id: String,
    val title: String,
    val description: String,
    val previewText: String,
    val previewPic: String,
    val mainPic: String,
    val typeTitle: String,
    val startDate: Long?,
    val placeTitle: String,
    val freeForPremium: Boolean,
    val price: Int,
    val linkToRegister: String,
    val status: String,
    val lon: Float?,
    val lat: Float?,
    val allowContent: Boolean,
    @EventType
    val type: String
) {
    annotation class EventType {
        companion object {
            const val EVENT = "EVENT"
            const val LONG_READ = "LONG_READ"
        }
    }
}