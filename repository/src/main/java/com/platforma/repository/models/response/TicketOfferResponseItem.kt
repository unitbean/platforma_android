package com.platforma.repository.models.response

import androidx.annotation.StringDef

class TicketOfferResponseItem(
    val id: String,
    val title: String,
    val description: String,
    val previewText: String,
    val previewPic: String,
    val mainPic: String,
    val logo: String,
    // format yyyy-MM-dd’T’HH:mm:ss.SSSXXX
    val date: String,
    val countOfFreeTickets: Int,
    val price: Int,
    val prevPrice: Int,
    @SendRequestStatus
    val didSendRequest: String,
    val allow: Boolean
) {

    @StringDef(NEW, APPROVED, REJECTED)
    @Retention(AnnotationRetention.SOURCE)
    annotation class SendRequestStatus

    companion object {

        const val NEW = "NEW"
        const val APPROVED = "APPROVED"
        const val REJECTED = "REJECTED"
    }
}