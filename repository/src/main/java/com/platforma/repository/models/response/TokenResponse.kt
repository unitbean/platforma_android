package com.platforma.repository.models.response

class TokenResponse(
    val token: String,
    val expiredAt: Long,
    val timeZone: String,
    val active: Boolean
)