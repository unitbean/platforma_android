package com.platforma.repository.models.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class BuyLinkResponse(
    val orderId: String,
    val successUrl: String,
    val errorUrl: String
): Parcelable