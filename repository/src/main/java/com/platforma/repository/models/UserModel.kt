package com.platforma.repository.models

import androidx.annotation.IntDef

data class UserModel(
    val id: String,
    val name: String?,
    val lastName: String?,
    val avatar: String?,
    @PremiumStatus
    val premiumStatus: Int,
    val endPremiumDate: String?
)

@Retention(AnnotationRetention.SOURCE)
@IntDef(PremiumStatus.NOT_PREMIUM, PremiumStatus.TRIAL, PremiumStatus.PREMIUM)
annotation class PremiumStatus {
    companion object {
        const val NOT_PREMIUM = 0
        const val TRIAL = 1
        const val PREMIUM = 2
    }
}