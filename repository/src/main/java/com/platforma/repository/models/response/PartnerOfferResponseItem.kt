package com.platforma.repository.models.response

class PartnerOfferResponseItem(
    val id: String,
    val title: String,
    val description: String,
    val previewText: String,
    val previewPic: String,
    val mainPic: String,
    val logo: String,
    val promoCode: String?,
    val allow: Boolean
)