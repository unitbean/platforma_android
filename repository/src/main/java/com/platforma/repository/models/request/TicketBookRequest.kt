package com.platforma.repository.models.request

class TicketBookRequest(
    val ticketId: String,
    val phone: String,
    val name: String,
    val count: Int = 1
)