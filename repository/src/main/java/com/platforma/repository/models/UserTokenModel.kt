package com.platforma.repository.models

import androidx.annotation.StringDef

class UserTokenModel(
    val token: String,
    val userId: String?,
    @TokenType
    val type: String) {

    @StringDef(TokenType.VK, TokenType.FB)
    @Retention(AnnotationRetention.SOURCE)
    annotation class TokenType {
        companion object {
            const val VK = "VK"
            const val FB = "FB"
        }
    }
}