package com.platforma.repository.profile

import com.platforma.repository.models.UserModel
import com.platforma.repository.models.UserTokenModel

interface IProfileRepository {
    fun logout()
    fun getUser(): UserModel?
    fun isUserLoaded(): Boolean
    fun getToken(): UserTokenModel?
    fun getTrialRemainingDays(): Long
    fun getEndPremiumDate(): String
    suspend fun updateUser(token: UserTokenModel): UserModel?
}