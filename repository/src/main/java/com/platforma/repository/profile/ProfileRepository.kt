package com.platforma.repository.profile

import com.platforma.repository.ApiService
import com.platforma.repository.UserService
import com.platforma.repository.models.UserModel
import com.platforma.repository.models.UserTokenModel

class ProfileRepository(private val apiService: ApiService, private val userService: UserService) : IProfileRepository {

    override fun getUser(): UserModel? = userService.user

    override suspend fun updateUser(token: UserTokenModel): UserModel? {
        return userService.forceUpdateUser(apiService.platforma)
    }

    override fun isUserLoaded(): Boolean = userService.userToken != null && userService.user != null

    override fun logout() {
        userService.cleanData()
    }

    override fun getToken(): UserTokenModel? = userService.userToken

    override fun getTrialRemainingDays(): Long = userService.getTrialRemainingDays()

    override fun getEndPremiumDate(): String = userService.getEndPremiumDate()
}