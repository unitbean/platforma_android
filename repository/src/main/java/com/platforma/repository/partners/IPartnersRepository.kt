package com.platforma.repository.partners

import com.platforma.repository.models.response.PartnerOfferResponseItem
import com.platforma.repository.models.response.TicketOfferResponseItem

interface IPartnersRepository {
    suspend fun loadPartners(): List<PartnerOfferResponseItem>
    suspend fun loadTickets(): List<TicketOfferResponseItem>
}