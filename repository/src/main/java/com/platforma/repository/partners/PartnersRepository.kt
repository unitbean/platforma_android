package com.platforma.repository.partners

import com.platforma.repository.ApiService
import com.platforma.repository.models.response.PartnerOfferResponseItem
import com.platforma.repository.models.response.TicketOfferResponseItem

class PartnersRepository(private val api: ApiService) : IPartnersRepository {

    override suspend fun loadPartners(): List<PartnerOfferResponseItem> = api.platforma.getPartners().result

    override suspend fun loadTickets(): List<TicketOfferResponseItem> = api.platforma.getTickets().result
}