package com.platforma.repository

import com.platforma.repository.models.BaseResponse
import com.platforma.repository.models.request.AuthRequest
import com.platforma.repository.models.request.PromocodeRequest
import com.platforma.repository.models.request.TicketBookRequest
import com.platforma.repository.models.response.*
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import java.util.concurrent.TimeUnit

class ApiService(
    serverUrl: String,
    deviceInterceptor: Interceptor,
    authInterceptor: Interceptor,
    loggingInterceptor: Interceptor
) {

    private val client: OkHttpClient by lazy {
        OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(15, TimeUnit.SECONDS)
            .addInterceptor(deviceInterceptor)
            .addInterceptor(authInterceptor)
            .addInterceptor(loggingInterceptor)
            .build()
    }

    val platforma: Main by lazy {
        Retrofit.Builder()
            .baseUrl(serverUrl)
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
            .create(Main::class.java)
    }

    interface Main {
        @GET("$VERSION/event")
        suspend fun getEvents(): BaseResponse<List<EventResponseItem>>

        @GET("$VERSION/partner")
        suspend fun getPartners(): BaseResponse<List<PartnerOfferResponseItem>>

        @GET("$VERSION/ticket")
        suspend fun getTickets(): BaseResponse<List<TicketOfferResponseItem>>

        @GET("$VERSION/subscriber/me")
        suspend fun updateUser(): BaseResponse<UserResponse>

        @POST("$VERSION/subscriber/auth")
        suspend fun getUser(@Body authRequest: AuthRequest): BaseResponse<AuthResponse>

        @GET("$VERSION/tariff")
        suspend fun getSubscriptions(): BaseResponse<List<SubscriptionResponse>>

        @POST("$VERSION/promo-code")
        suspend fun sendPromocodeAndCheck(@Body request: PromocodeRequest): BaseResponse<UserResponse>

        @POST("$VERSION/ticket/registration")
        suspend fun bookTicket(@Body request: TicketBookRequest): BaseResponse<String>

        @GET("$VERSION/order/{id}/init")
        suspend fun startBuy(@Path("id") subscriptionId: String): BaseResponse<BuyLinkResponse>
    }

    companion object {
        private const val VERSION = "/api/v1"
    }
}