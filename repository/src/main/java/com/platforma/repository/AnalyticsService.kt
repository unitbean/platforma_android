package com.platforma.repository

import android.app.Application
import android.content.Context
import android.os.Bundle
import com.facebook.appevents.AppEventsConstants
import com.facebook.appevents.AppEventsLogger
import com.yandex.metrica.Revenue
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig
import java.math.BigDecimal
import java.util.*


class AnalyticsService(context: Context) {

    private val facebookLogger: AppEventsLogger by lazy { AppEventsLogger.newLogger(context) }

    init {
        val config = YandexMetricaConfig.newConfigBuilder(context.getString(R.string.yandex_app_id)).build()
        YandexMetrica.activate(context, config)

        if (context is Application) {
            YandexMetrica.enableActivityAutoTracking(context)
        }
    }

    fun logPurchase(priceInMicros: Long, productId: String) {
        YandexMetrica.reportEvent(EVENT_PURCHASE)
        YandexMetrica.reportRevenue(
            Revenue
                .newBuilderWithMicros(priceInMicros, Currency.getInstance("RUB"))
                .withProductID(productId)
                .build()
        )
    }

    fun logEvent(key: String, value: Map<String, Any>? = null) {
        YandexMetrica.reportEvent(key, value)
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    fun logCompleteRegistrationEvent(registrationMethod: String) {
        val params = Bundle()
        params.putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, registrationMethod)
        facebookLogger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, params)
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    fun logViewContentEvent(
        contentType: String,
        contentData: String,
        contentId: String,
        price: Double
    ) {
        val params = Bundle().apply {
            putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType)
            putString(AppEventsConstants.EVENT_PARAM_CONTENT, contentData)
            putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, contentId)
            putString(AppEventsConstants.EVENT_PARAM_CURRENCY, "RUB")
        }
        facebookLogger.logEvent(AppEventsConstants.EVENT_NAME_VIEWED_CONTENT, price, params)
    }

    /**
     * logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     * purchaseAmount is BigDecimal, e.g. BigDecimal.valueOf(4.32).
     * currency is Currency, e.g. Currency.getInstance("USD"),
     *     where the string is from http://en.wikipedia.org/wiki/ISO_4217.
     * parameters is Bundle.
     */
    fun logPurchaseComplete(
        purchaseAmount: Double,
        contentType: String,
        contentData: String,
        contentId: String) {
        val parameters = Bundle().apply {
            putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType)
            putString(AppEventsConstants.EVENT_PARAM_CONTENT, contentData)
            putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, contentId)
        }
        facebookLogger.logPurchase(BigDecimal.valueOf(purchaseAmount), Currency.getInstance("RUB"), parameters)
    }

    companion object {
        const val EVENT_LOGIN = "login"
        const val EVENT_LOGIN_TYPE = "type"
        const val EVENT_LOGIN_TYPE_VK = "vk"
        const val EVENT_LOGIN_TYPE_FB = "fb"

        const val SCR_OPEN_FEED = "SCR_OPEN_FEED"
        const val SCR_OPEN_EVENT = "SCR_OPEN_EVENT"
        const val SCR_OPEN_MY_EVENTS = "SCR_OPEN_MY_EVENTS"
        const val SCR_OPEN_PARTNERS = "SCR_OPEN_PARTNERS"
        const val SCR_OPEN_PARTNER = "SCR_OPEN_PARTNER"
        const val SCR_OPEN_TICKET = "SCR_OPEN_TICKET"
        const val SCR_OPEN_CLUB = "SCR_OPEN_CLUB"
        const val SCR_OPEN_LOGIN = "SCR_OPEN_LOGIN"
        const val SCR_OPEN_PROFILE = "SCR_OPEN_PROFILE"
        const val SCR_OPEN_PAYMENT_SUCCESS = "SCR_OPEN_PAYMENT_SUCCESS"
        const val SCR_OPEN_PAYMENT_ERROR = "SCR_OPEN_PAYMENT_ERROR"
        const val SCR_OPEN_PROMOCODE = "SCR_OPEN_PROMOCODE"

        const val FB_PURCHASE_AMOUNT = "PURCHASE_AMOUNT"
        const val FB_CONTENT_TYPE = "CONTENT_TYPE"
        const val FB_CONTENT_DATA = "CONTENT_DATA"
        const val FB_CONTENT_ID = "CONTENT_ID"
        const val FB_CONTENT_PRICE = "CONTENT_PRICE"

        private const val EVENT_PURCHASE = "purchase"
    }
}