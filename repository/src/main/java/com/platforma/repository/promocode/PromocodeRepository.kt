package com.platforma.repository.promocode

import com.platforma.repository.ApiService
import com.platforma.repository.UserService
import com.platforma.repository.models.UserModel

class PromocodeRepository(private val api: ApiService, private val userService: UserService) : IPromocodeRepository {
    override suspend fun sendPromocodeOnServer(promocode: String): UserModel {
        return userService.checkPromocodeAndSaveUser(api, promocode)
    }
}