package com.platforma.repository.promocode

import com.platforma.repository.models.UserModel

interface IPromocodeRepository {
    suspend fun sendPromocodeOnServer(promocode: String): UserModel
}